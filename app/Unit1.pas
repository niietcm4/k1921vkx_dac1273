unit Unit1;

//////{$Define DBGMODE}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, Vcl.ExtCtrls, VCLTee.TeeProcs,
  VCLTee.Chart, Vcl.ComCtrls, Vcl.Buttons, VCLTee.TeeFunci, VCLTee.Series,math,PortUnit,
  Vcl.Grids;

Const
  noError            = 0;
  ReadError          = 1;
  HeaderError        = 2;
  DataError          = 3;
  FileCorrupt        = 4;
  IncorectFileFormat = 5;

const FcfgNAME='niiet_dac1273.cfg';
      CONF_PORT='PORT=';
      CONF_TIMEOUT='TIMEOUT=';
      CONF_BADD='BUFF_PACKEGE=';
      DAC_CLK_MAX =100;
      PintCount_Default = 100;
type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    wav1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    OpenDialog1: TOpenDialog;
    Memo1: TMemo;
    GroupBox1: TGroupBox;
    StatusBar1: TStatusBar;
    SaveDialog1: TSaveDialog;
    Chart1: TChart;
    Series1: TFastLineSeries;
    TeeFunction1: TCustomTeeFunction;
    Series2: TFastLineSeries;
    TeeFunction2: TAverageTeeFunction;
    GroupBox2: TGroupBox;
    Lpointcount0: TLabel;
    Btn_load0: TBitBtn;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    N5: TMenuItem;
    SIN1: TMenuItem;
    Meandr1: TMenuItem;
    Nsin_ch0: TMenuItem;
    Nsin_ch1: TMenuItem;
    Nsin_2channel: TMenuItem;
    Lpointcnt0: TLabel;
    Lpointcnt1: TLabel;
    Nconnect: TMenuItem;
    Btn_load_buff: TBitBtn;
    Label4: TLabel;
    cb_comport: TComboBox;
    PopupMenu1: TPopupMenu;
    N_dac_reg: TMenuItem;
    N6: TMenuItem;
    GroupBox5: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    L_period0: TLabel;
    L_frieq0: TLabel;
    N01: TMenuItem;
    N11: TMenuItem;
    N7: TMenuItem;
    SaveDialog2: TSaveDialog;
    Rise1: TMenuItem;
    N02: TMenuItem;
    N12: TMenuItem;
    N8: TMenuItem;
    riangle1: TMenuItem;
    N03: TMenuItem;
    N13: TMenuItem;
    N9: TMenuItem;
    Timer1: TTimer;
    N14: TMenuItem;
    Label9: TLabel;
    cb_spi_speed: TComboBox;
    Label10: TLabel;
    rg_dac_load: TRadioGroup;
    cb_dac_sync: TCheckBox;
    GroupBox4: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    L_period1: TLabel;
    L_frieq1: TLabel;
    N10: TMenuItem;
    N15: TMenuItem;
    Btn_dac_stop: TBitBtn;
    Btn_dac_start: TBitBtn;
    GroupBox6: TGroupBox;
    Bit_dac_msb: TBitBtn;
    Label1: TLabel;
    Label5: TLabel;
    N_full: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    cb_device: TComboBox;
    Label6: TLabel;
    L_timeout: TLabel;
    NDemo: TMenuItem;
    cb_shapes_ch0: TComboBox;
    cb_shapes_ch1: TComboBox;
    Bit_dac_rst: TBitBtn;
    cb_dac_msb: TCheckBox;
    cb_015_out: TComboBox;
    L_015_out: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure wav1Click(Sender: TObject);
    procedure Nsin_ch0Click(Sender: TObject);
    procedure Nsin_ch1Click(Sender: TObject);
    procedure Nsin_2channelClick(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure Btn_load0Click(Sender: TObject);
    procedure Btn_load_buffClick(Sender: TObject);
    procedure NconnectClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N6Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N01Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N02Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N03Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure cb_spi_speedChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure Bit_dac_msbClick(Sender: TObject);
    procedure Btn_dacClick(Sender: TObject);
    procedure Btn_dac_startClick(Sender: TObject);
    procedure Btn_dac_stopClick(Sender: TObject);
    procedure rg_dac_loadClick(Sender: TObject);
    procedure N_fullClick(Sender: TObject);
//    procedure Rise1Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure cb_deviceChange(Sender: TObject);
    procedure L_timeoutClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NDemoClick(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure cb_shapes_ch0Click(Sender: TObject);
    procedure cb_shapes_ch1Click(Sender: TObject);
    procedure L_frieq0Click(Sender: TObject);
    procedure L_frieq1Click(Sender: TObject);
    procedure Bit_dac_rstClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

TWaveHeader = record
    idRiff: array[0..3] of char;
    RiffLen: longint;
    idWave: array[0..3] of char;
    idFmt: array[0..3] of char;
    InfoLen: longint;
    WaveType: smallint;
    Ch: smallint;
    Freq: longint;
    BytesPerSec: longint;
    align: smallint;
    Bits: smallint;
  end;
  TDataHeader = record
    idData: array[0..3] of char;
    DataLen: longint;
  end;

  TWaveHeaderChank = record     //�������� (����) �������
    wFormatTag     : Smallint;
    wChannels      : WORD;
    wSamplesPerSec : Cardinal;
    wAvgBytesPerSec: Cardinal;
    wBlockAlign    : WORD;
    wBitsPerSample : WORD;
    wcbSize        : WORD;
  end;

  TWaveResult = record          //����������� ���������
    ERROR          : WORD;      //������������ ��� ����������� ����������
    wAvgBytesPerSec: Cardinal;  //������� Wav �����
    wBitsPerSample : WORD;
    wChannels      : WORD;
    Data           : TMemoryStream;
  end;

  TDAC_Channel_Data = record
    ch0:Word;
    ch1:Word;
  end;

  TDAC_Data = record
    data:array[1..65536] of TDAC_Channel_Data;
    count0,count1:integer;
    repeat0,repeat1:integer;
    shape0,shape1:string;
    min0,min1:Int64;
    max0,max1:Int64;
  end;

  TDAC_Params = record
    Name:string;
    reg,bpos,blen:byte;
    str_val:string;
  end;

  TDAC_Device = record
    Name:string;
    config:boolean;
  end;

  // ��������� ?����� ��������� wav-������
const DAC_MAX_DIGIT=16;
      DAC_INFO ='%INFO';
      DAC_INFO_ANSWER ='K1921VK01T_DAC1273';
      DAC_INFO_ANSWER2 ='K1921VK035_DAC1273';

      DAC_BUFF_MAXPOINT ='%BMPOINT';
      DAC_BUFF_MAXPOINT_ANSWER ='Max Point: ';
      DAC_BUFF_ADD   = '%BADD ';
      DAC_BUFF_ADDN   = '%BADDN ';
      DAC_BUFF_READ  = '%BGET ';
      DAC_BUFF_ALL_READ  = '%BAGET ';
      DAC_BUFF_SIZE  = '%BSIZE ';
      DAC_BUFF_DIV   = '%BDIV ';
      DAC_BUFF_COUNT = '%BCOUNT';
      DAC_BUFF_CRC32 = '%BCRC';
      DAC_BUFF_CRC32_ANSWER = 'BCRC = ';
      DAC_BUFF_UPDATE= '%BUPDT ';
      DAC_BUFF_START=  '%BSTRT ';
      DAC_BUFF_STOP=   '%BSTP ';
      DAC_BUFF_UPDATE_ANSWER= 'BUFF UPDATED';
      DAC_REG_WRITE  = '%WREG ';
      DAC_REG_READ   = '%RREG ';
      DAC_REG_READ_ANSWER = 'RREG ';
      DAC_REG_READ_ANSWER_DATA = ' = ';
      DAC_REG_READB  = '%RREGB ';
      DAC_REG_READB_ANSWER = 'RREGB ';
      DAC_BUFF_READ_ANSWER = 'BGET ';
      DAC_MODE_PARALLEL = '%DMLD ';
      DAC_UPDATE_SYNC = '%DUPSN ';
      DAC_MSB = '%DMSB ';
      DAC_MSB_ANSWER_SET = 'MSB En';
      DAC_MSB_ANSWER_RESET = 'MSB Dis';
      DAC_RESET = '%DRST ';
      SPI_SET_SPEED = '%SSSPEED ';
      DAC_DEVICE = '%DDSEL ';
      DAC_OVERFLOW_ANSWER = 'Out of Max Count Point';
      DAC_REG_PARAMS : array[1..36] of TDAC_Params = ((Name:'��������� PLL_LOCK'  ;reg:0;bpos:1;blen:1;str_val:'OFF;ON'),
                                                      (Name:'����� 1R/2R'         ;reg:0;bpos:2;blen:1;str_val:'1R;2R'),
                                                      (Name:'����� ����� ��������';reg:0;bpos:3;blen:1;str_val:'OFF;ON'),
                                                      (Name:'����� �����������'   ;reg:0;bpos:4;blen:1;str_val:'OFF;ON'),
                                                      (Name:'�����'               ;reg:0;bpos:5;blen:1;str_val:'OFF;ON'),
                                                      (Name:'SPI_LSB_MSB'         ;reg:0;bpos:6;blen:1;str_val:'MSB;LSB'),
                                                      (Name:'SDIO'                ;reg:0;bpos:7;blen:1;str_val:'In;In/Out'),

                                                      (Name:'DATACLK_PLL_LOCK'    ;reg:1;bpos:0;blen:1;str_val:'PLL_LOCK;DATACLK'),
                                                      (Name:'e^jwt'               ;reg:1;bpos:1;blen:1;str_val:'e^-jwt ;e^+jwt'),
                                                      (Name:'����������'          ;reg:1;bpos:2;blen:1;str_val:'Real;Cplx'),
                                                      (Name:'���������� ������'   ;reg:1;bpos:3;blen:1;str_val:'OFF;ON'),
                                                      (Name:'����� ���������'     ;reg:1;bpos:4;blen:2;str_val:'OFF;f_dac/2;f_dac/4;f_dac/8'),
                                                      (Name:'����������� ��������';reg:1;bpos:6;blen:2;str_val:'1x;2x;4x;8x'),

                                                      (Name:'I ��� Q ������'      ;reg:2;bpos:0;blen:1;str_val:'I;Q'),
                                                      (Name:'IQSEL ��������'      ;reg:2;bpos:1;blen:1;str_val:'OFF;ON'),
                                                      (Name:'ONEPORTCLK ��������' ;reg:2;bpos:2;blen:1;str_val:'OFF;ON'),
                                                      (Name:'DATACLK  ��������'   ;reg:2;bpos:4;blen:1;str_val:'OFF;ON'),
                                                      (Name:'DATACLK  ��������'   ;reg:2;bpos:5;blen:1;str_val:'OFF;ON'),
                                                      (Name:'���������� ������'   ;reg:2;bpos:6;blen:1;str_val:'2;1'),
                                                      (Name:'���'                 ;reg:2;bpos:7;blen:1;str_val:'���.;������'),

                                                      (Name:'����.������� PLL'    ;reg:3;bpos:0;blen:2;str_val:'0;1;2;3'),
                                                      (Name:'�������� CLK'        ;reg:3;bpos:7;blen:1;str_val:'DATACLK;SDO'),

                                                      (Name:'PLL ���������� ������';reg:4;bpos:0;blen:3;str_val:'seq'),
                                                      (Name:'PLL ������� ���������� ������';reg:4;bpos:6;blen:1;str_val:'Auto;Programm'),
                                                      (Name:'PLL On/Off'           ;reg:4;bpos:7;blen:1;str_val:'OFF;ON'),

                                                      (Name:'IDAC �������� ������' ;reg:5;bpos:0;blen:8;str_val:'seq'),
                                                      (Name:'IDAC ��������  �����' ;reg:6;bpos:0;blen:4;str_val:'seq'),
                                                      (Name:'IDAC ��������2'       ;reg:7;bpos:0;blen:8;str_val:'seq'),
                                                      (Name:'IDAC ��������1'       ;reg:8;bpos:0;blen:2;str_val:'seq'),
                                                      (Name:'IDAC OFFSET'          ;reg:8;bpos:7;blen:1;str_val:'IoutA;IoutB'),

                                                      (Name:'QDAC �������� ������' ;reg:9;bpos:0;blen:8;str_val:'seq'),
                                                      (Name:'QDAC ��������  �����' ;reg:10;bpos:0;blen:4;str_val:'seq'),
                                                      (Name:'QDAC ��������2'       ;reg:11;bpos:0;blen:8;str_val:'seq'),
                                                      (Name:'QDAC ��������1'       ;reg:12;bpos:0;blen:2;str_val:'seq'),
                                                      (Name:'QDAC OFFSET'          ;reg:12;bpos:7;blen:1;str_val:'IoutA;IoutB'),

                                                      (Name:'������'               ;reg:13;bpos:0;blen:4;str_val:'seq'));
  //SPI_SPEED:array[1..6] of string = ('50','25','12','6','2','1');
  SPI_SPEED:array[1..6] of string = ('25','12','6','2','1','0.5');
 // DAC_SPEED:array[1..6] of integer = (769230,564971,362318,226757,44984,85689);
  //DAC_SPEED:array[1..6] of integer = (1515000,925800,540600,295000,100300,51310);
  DAC_SPEED:array[1..6] of integer = (925800,540600,295000,100300,51310,25650);
  DAC_SPEED_034:array[1..6] of integer = (714200,413200,244000,84500,44000,22000);
  DAC_SPEED_025:array[1..6] of integer = (840500,523500,298600,108000,55800,28000);
  DAC_SPEED_015:array[1..6] of integer = (680500,392200,220800,76000,38500,19300);
  FILE_LOG_NAME='niiet_dac1273na03A4.log';
var
  Form1: TForm1;
  DAC_Data:TDAC_Data;
  DAC_Data_out:TDAC_Data;
  PintCountLast:Int64;
  mass_str:array[0..7] of String;
  nmass_str:byte;
  DAC_Devices:array[0..2] of TDAC_Device;
  DAC_Device_count:byte;
  DAC_DeviceCurrent:byte;
  DAC_MaxPoint:Integer;
  File_log:TextFile;
  DAC_Buff_Pack:boolean;
//  ComPortName:string;

  procedure Add_log(a:string);
  function HEX2DEC(sHEX: string): Int64;
  function DEC2HEX(DEC: Int64): string;

implementation
{$R *.dfm}

uses Unit2;
var CRCtable: array[0..255] of cardinal;
    sg_dac_regs_Acol,sg_dac_regs_Arow:Integer;
    buff_count:integer;

procedure Add_log(a:string);
var s:string;
begin
  s:=TimeToStr(Now)+': '+a;
  Form1.Memo1.Lines.Add(s);
  Append(File_log);
  WriteLn(File_log,s);
  CloseFile(File_log);
end;

function StrToArrays(str, r: string): Boolean;
var
  j: integer;
begin
  if str <> '' then
  begin
    nmass_str:=0;
    while str <> '' do
    begin
      j := Pos(r,str);
      if j=0 then
        j := Length(str) + 1;
      mass_str[nmass_str]:=Copy(Str,1,j-1);
      inc(nmass_str);
      Delete(Str,1,j+length(r)-1);
    end;
    Result:=True;
  end
  else
    Result:=False;
end;

Procedure MCU_Connected();
begin
// Form1.StatusBar1.Panels[0].
end;

function GetCRC(OldCRC: cardinal; StPtr: pointer; StLen: integer): cardinal;
asm
  test edx,edx;
  jz @ret;
  neg ecx;
  jz @ret;
  sub edx,ecx; // Address after last element

  push ebx;
  mov ebx,0; // Set ebx=0 & align @next
@next:
  mov bl,al;
  xor bl,byte [edx+ecx];
  shr eax,8;
  xor eax,cardinal [CRCtable+ebx*4];
  inc ecx;
  jnz @next;
  pop ebx;

@ret:
end;


//---------------------------------
// Function Convertion numbers
//---------------------------------

function IntToBinStr(Value: Integer): string;
type
SmallStr = array[0..3] of char;
const
Str: array[0..15] of SmallStr =
(
'0000', '0001', '0010', '0011',
'0100', '0101', '0110', '0111',
'1000', '1001', '1010', '1011',
'1100', '1101', '1110', '1111');
var bts: array[0..3] of byte absolute value;
ResultArr: array[0..32] of char;
begin
SmallStr((@ResultArr[0])^) := Str[(bts[3] and $F0) shr 4];
SmallStr((@ResultArr[4])^) := Str[bts[3] and $F];
SmallStr((@ResultArr[8])^) := Str[(bts[2] and $F0) shr 4];
SmallStr((@ResultArr[12])^) := Str[bts[2] and $F];
SmallStr((@ResultArr[16])^) := Str[(bts[1] and $F0) shr 4];
SmallStr((@ResultArr[20])^) := Str[bts[1] and $F];
SmallStr((@ResultArr[24])^) := Str[(bts[0] and $F0) shr 4];
SmallStr((@ResultArr[28])^) := Str[bts[0] and $F];
ResultArr[32] := #0;
Result := ResultArr;
end;

function DEC2BIN(DEC: LONGINT): string;

var
  BIN: string;
  I, J: LONGINT;

begin
  if DEC = 0 then
    BIN := '0'
  else
  begin
    BIN := '';
    I := 0;
    while ((1 shl (I + 1)) <= DEC) do
      I := I + 1;
    { (1 SHL (I + 1)) = 2^(I + 1) }
    for J := 0 to I do
    begin
      if (DEC shr (I - J)) = 1 then
        BIN := BIN + '1'
          { (DEC SHR (I - J)) = DEC DIV 2^(I - J) }
      else
        BIN := BIN + '0';
      DEC := DEC and ((1 shl (I - J)) - 1);
      { DEC AND ((1 SHL (I - J)) - 1) = DEC MOD 2^(I - J) }
    end;
  end;
  DEC2BIN := BIN;
end;

function BIN2DEC(a: string): Int64;
var
  J: LONGINT;
  Error: BOOLEAN;
  DEC: Int64;
  BIN:string;
begin
  DEC := 0;
  BIN:=a;
  while(pos('_',BIN)>0) do delete(BIN,pos('_',BIN),1);
  Error := False;
  for J := 1 to Length(BIN) do
  begin
    if (BIN[J] <> '0') and (BIN[J] <> '1') then
      Error := True;
    if BIN[J] = '1' then
      DEC := DEC + (1 shl (Length(BIN) - J));
    { (1 SHL (Length(BIN) - J)) = 2^(Length(BIN)- J) }
  end;
  if Error then
    BIN2DEC := 0
  else
    BIN2DEC := DEC;
end;

function BIN2HEX(BIN: string): string;

  function SetHex(St: string; var Error: BOOLEAN): CHAR;

  var
    Ch: CHAR;

  begin
    if St = '0000' then
      Ch := '0'
    else if St = '0001' then
      Ch := '1'
    else if St = '0010' then
      Ch := '2'
    else if St = '0011' then
      Ch := '3'
    else if St = '0100' then
      Ch := '4'
    else if St = '0101' then
      Ch := '5'
    else if St = '0110' then
      Ch := '6'
    else if St = '0111' then
      Ch := '7'
    else if St = '1000' then
      Ch := '8'
    else if St = '1001' then
      Ch := '9'
    else if St = '1010' then
      Ch := 'A'
    else if St = '1011' then
      Ch := 'B'
    else if St = '1100' then
      Ch := 'C'
    else if St = '1101' then
      Ch := 'D'
    else if St = '1110' then
      Ch := 'E'
    else if St = '1111' then
      Ch := 'F'
    else
      Error := True;
    SetHex := Ch;
  end;

var
  HEX: string;
  I: INTEGER;
  Temp: string[4];
  Error: BOOLEAN;

begin
  Error := False;
  if BIN = '0' then
    HEX := '0'
  else
  begin
    Temp := '';
    HEX := '';
    if Length(BIN) mod 4 <> 0 then
      repeat
        BIN := '0' + BIN;
      until Length(BIN) mod 4 = 0;
    for I := 1 to Length(BIN) do
    begin
      Temp := Temp + BIN[I];
      if Length(Temp) = 4 then
      begin
        HEX := HEX + SetHex(Temp, Error);
        Temp := '';
      end;
    end;
  end;
  if Error then
    BIN2HEX := '0'
  else
    BIN2HEX := HEX;
end;

function HEX2BIN(HEX: string): string;
var
  BIN: string;
  I: INTEGER;
  Error: BOOLEAN;

begin
  Error := False;
  BIN := '';
  for I := 1 to Length(HEX) do
    case UpCase(HEX[I]) of
      '0': BIN := BIN + '0000';
      '1': BIN := BIN + '0001';
      '2': BIN := BIN + '0010';
      '3': BIN := BIN + '0011';
      '4': BIN := BIN + '0100';
      '5': BIN := BIN + '0101';
      '6': BIN := BIN + '0110';
      '7': BIN := BIN + '0111';
      '8': BIN := BIN + '1000';
      '9': BIN := BIN + '1001';
      'A': BIN := BIN + '1010';
      'B': BIN := BIN + '1011';
      'C': BIN := BIN + '1100';
      'D': BIN := BIN + '1101';
      'E': BIN := BIN + '1110';
      'F': BIN := BIN + '1111';
    else
      Error := True;
    end;
  if Error then
    HEX2BIN := '0'
  else
    HEX2BIN := BIN;
end;

function DEC2HEX(DEC: Int64): string;
const
  HEXDigts: string[16] = '0123456789ABCDEF';

var
  HEX: string;
  I, J: byte;
begin
  if ((DEC = 0) OR (DEC < 0)) then
    HEX := '0'
  else
  begin
    HEX := '';
    I := 0;
    while ((power(16,(I+1)) <= DEC) AND (I<64)) do inc(I);// := I + 1;
    { 16^N = 2^(N * 4) }
    { (1 SHL ((I + 1) * 4)) = 16^(I + 1) }
    for J := 0 to I do
    begin
      HEX := HEX + HEXDigts[(DEC shr ((I - J) * 4)) + 1];
      { (DEC SHR ((I - J) * 4)) = DEC DIV 16^(I - J) }
      DEC := DEC and ((1 shl ((I - J) * 4)) - 1);
      { DEC AND ((1 SHL ((I - J) * 4)) - 1) = DEC MOD 16^(I - J) }
    end;
  end;
  DEC2HEX := HEX;
end;

function HEX2DEC(sHEX: string): Int64;
  function Digt(Ch: CHAR): BYTE;
  const
    HEXDigts: string[16] = '0123456789ABCDEF';
  var
    I: BYTE;
    N: BYTE;
  begin
    N := 0;
    for I := 1 to Length(HEXDigts) do
      if (ord(Ch) = ord(HEXDigts[I])) then
        N := I - 1;
    Digt := N;
  end;

//const
//  HEXSet: set of CHAR = ['0'..'9', 'A'..'F'];
var
  HEX: string;
  J: integer;
  Error: BOOLEAN;
  DEC,d: Int64;
begin
  DEC := 0;
  Error := False;
  HEX:=sHEX;
  while(pos('_',HEX)>0) do delete(HEX,pos('_',HEX),1);
  for J := 1 to Length(HEX) do
  if (HEX[J]<>'0') then
  begin
    if (not CharInSet(UpCase(HEX[J]), ['0'..'9', 'A'..'F'])) then
      Error := True;
      d:= trunc(Digt(UpCase(HEX[J])) * power(16,(Length(HEX)-J)));
    DEC := DEC + d;//Digt(UpCase(HEX[J])) shl ((Length(HEX) - J) * 4);
    { 16^N = 2^(N * 4) }
    { N SHL ((Length(HEX) - J) * 4) = N * 16^(Length(HEX) - J) }
  end;
  if Error then
    HEX2DEC := 0
  else
    HEX2DEC := DEC;
end;

function get_dataval(a:string):Int64;
begin
//  if (a[1]='') then
  if ((a[length(a)]='h') OR (a[length(a)]='H')) then result:=HEX2DEC(copy(a,1,length(a)-1)) else
  if ((a[length(a)]='b') OR (a[length(a)]='B')) then result:=BIN2DEC(copy(a,1,length(a)-1)) else
  result:=strtoint(a);
end;

function normalize_addr(a:string; size:byte):string;
var s:string;
begin
  s:=a;
  while (length(s)<size*2) do Insert('0',s,1);
  Result:=s;
end;

procedure SaveConfig();
var f:TextFile;
    s:string;
begin
  AssignFile(f,ExtractFilePath(Application.ExeName)+FcfgNAME);
  Rewrite(f);
  Writeln(f,CONF_PORT+Form1.cb_comport.Text);
  if (PortTimeOut>0) then Writeln(f,CONF_TIMEOUT+inttostr(PortTimeOut));
  if (DAC_Buff_Pack) then Writeln(f,CONF_BADD+'1') else Writeln(f,CONF_BADD+'0');
  CloseFile(f);
end;

procedure LoadConfig();
var f:TextFile;
    s,tmp:string;
begin
  AssignFile(f,ExtractFilePath(Application.ExeName)+FcfgNAME);
  if (FileExists(ExtractFilePath(Application.ExeName)+FcfgNAME)) then
  begin
    Reset(f);
    while(Not EOF(f)) do
    begin
      Readln(f,s);
      if (pos(CONF_PORT,s)>0) then Form1.cb_comport.Text:=copy(s,pos(CONF_PORT,s)+length(CONF_PORT),length(s)-pos(CONF_PORT,s)-length(CONF_PORT)+1);
      if (pos(CONF_TIMEOUT,s)>0) then PortTimeOut:=strtoint(copy(s,pos(CONF_TIMEOUT,s)+length(CONF_TIMEOUT),length(s)-pos(CONF_TIMEOUT,s)-length(CONF_TIMEOUT)+1));
      if (pos(CONF_BADD,s)>0) then
      begin
       tmp:=copy(s,pos(CONF_BADD,s)+length(CONF_BADD),length(s)-pos(CONF_BADD,s)-length(CONF_BADD)+1);
       if (tmp='1') then DAC_Buff_Pack:=True else DAC_Buff_Pack:=False;

      end;

    end;
    CloseFile(f);
  end else SaveConfig();
end;

procedure DacChannelSetCount(ach:byte);
begin
  if (ach = 2) then
  begin
    Form1.GroupBox4.Show;
    Form1.Nsin_ch1.Enabled:=True;
    Form1.Nsin_2channel.Enabled:=True;
    Form1.N11.Enabled:=True;
    Form1.N7.Enabled:=True;
    Form1.N12.Enabled:=True;
    Form1.N8.Enabled:=True;
    Form1.N13.Enabled:=True;
    Form1.N9.Enabled:=True;
    Form1.N17.Enabled:=True;
    Form1.N18.Enabled:=True;
    Form1.rg_dac_load.Enabled:=True;
    Form1.cb_dac_sync.Enabled:=True;
    Form1.Chart1.Series[1].Visible:=True;
    Form1.Bit_dac_msb.Enabled:=True;
    Form1.cb_dac_msb.Visible:=True;
    Form1.Bit_dac_rst.Visible:=True;
  end else
  begin
    Form1.GroupBox4.Hide;
    Form1.Nsin_ch1.Enabled:=False;
    Form1.Nsin_2channel.Enabled:=False;
    Form1.N11.Enabled:=False;
    Form1.N7.Enabled:=False;
    Form1.N12.Enabled:=False;
    Form1.N8.Enabled:=False;
    Form1.N13.Enabled:=False;
    Form1.N9.Enabled:=False;
    Form1.N17.Enabled:=False;
    Form1.N18.Enabled:=False;
    Form1.rg_dac_load.Enabled:=False;
    Form1.cb_dac_sync.Enabled:=False;
    Form1.Chart1.Series[1].Visible:=False;
    Form1.Bit_dac_msb.Enabled:=False;
    Form1.cb_dac_msb.Visible:=False;
    Form1.Bit_dac_rst.Visible:=False;
  end;
end;

procedure DACSelectDev();
begin

  if ((Form1.cb_device.ItemIndex >= 0) AND {(NOT BufferRunning) AND }(McuConnected)) then
  begin
    WriteStringComm(DAC_DEVICE+inttostr(Form1.cb_device.ItemIndex+1)+#13);Wait_Rx();
    if (Form1.cb_device.ItemIndex = 2) then DacChannelSetCount(2)
      else DacChannelSetCount(1);
  end;
  if (Form1.cb_device.ItemIndex = 0) then
  begin
    Form1.L_015_out.Show;
    Form1.cb_015_out.Show;
  end else
  begin
    Form1.L_015_out.Hide;
    Form1.cb_015_out.Hide;
  end;

  if (Form1.cb_device.ItemIndex = 2) then
  begin
    Form1.cb_dac_msb.Visible:=True;
    Form1.Bit_dac_rst.Visible:=True;
    Form1.rg_dac_load.Enabled:=True;
    Form1.Chart1.Legend.Show;
  end else
  begin
    Form1.cb_dac_msb.Visible:=False;
    Form1.Bit_dac_rst.Visible:=False;
    Form1.rg_dac_load.Enabled:=False;
    Form1.Chart1.Legend.Hide;
  end;
end;

function Get_frieqHz(ch:byte):Integer;
var x:Real;
    frieq:Int64;
begin
  if (DAC_DeviceCurrent = DAC_1273HA015_NUM) then frieq:=DAC_SPEED_015[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA025_NUM) then frieq:=DAC_SPEED_025[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then frieq:=DAC_SPEED_034[Form1.cb_spi_speed.ItemIndex+1]
  else frieq:=DAC_SPEED[Form1.cb_spi_speed.ItemIndex+1];
  if (Form1.rg_dac_load.ItemIndex = 0) then x:=2 else x:=1;
  if ((DAC_DeviceCurrent = DAC_1273HA015_NUM) OR (DAC_DeviceCurrent = DAC_1273HA025_NUM)) then x:=1;
  Result:=0;
  if ((ch=0) AND (DAC_Data.count0> 0)) then Result:= trunc(frieq/(DAC_Data.count0*x))
  else if ((ch=1) AND (DAC_Data.count1> 0)) then Result:= trunc(frieq/(DAC_Data.count1*x));
end;

function Frieq2Count(ch_frieq:real):Integer;
var x:Real;
    frieq:Int64;
begin
  if (DAC_DeviceCurrent = DAC_1273HA015_NUM) then frieq:=DAC_SPEED_015[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA025_NUM) then frieq:=DAC_SPEED_025[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then frieq:=DAC_SPEED_034[Form1.cb_spi_speed.ItemIndex+1]
  else frieq:=DAC_SPEED[Form1.cb_spi_speed.ItemIndex+1];
  if (Form1.rg_dac_load.ItemIndex = 0) then x:=2 else x:=1;
  if ((DAC_DeviceCurrent = DAC_1273HA015_NUM) OR (DAC_DeviceCurrent = DAC_1273HA025_NUM)) then x:=1;
  Result:=trunc(frieq/(x*ch_frieq));
end;

function Get_frieq(a:Real):String;
const ED_IZM:array [0..2] of String = (' ��',' ���',' ���');
var n_ed:byte;
    x:Real;
begin
  n_ed:=0;
  x:=a;
  while ((trunc(x) > 1000) AND (n_ed < HIGH(ED_IZM))) do
  begin
    x:=x/1000;
    inc(n_ed);
  end;
  Result:=Floattostrf(x,ffFixed,6,3)+ED_IZM[n_ed];
end;

function Get_period(a:Real):String;
const ED_IZM:array [0..2] of String = (' �',' ��',' ���');
var n_ed:byte;
    x:Real;
begin
  n_ed:=0;
  x:=a;
  while ((trunc(x) < 1) AND (n_ed < HIGH(ED_IZM))) do
  begin
    x:=x*1000;
    inc(n_ed);
  end;
  Result:=Floattostrf(x,ffFixed,6,3)+ED_IZM[n_ed];
end;

procedure Signal_calc(a:string);
//const DAC_SPI_BIT_COUNT = 20;
var data_cnt,x:Word;
    frieq,period:Int64;
begin
//  if (DAC_Data.count0 > DAC_Data.count1) then data_cnt:=DAC_Data.count0 else data_cnt:=DAC_Data.count1;
  if (DAC_DeviceCurrent = DAC_1273HA015_NUM) then frieq:=DAC_SPEED_015[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA025_NUM) then frieq:=DAC_SPEED_025[Form1.cb_spi_speed.ItemIndex+1]
  else if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then frieq:=DAC_SPEED_034[Form1.cb_spi_speed.ItemIndex+1]
  else frieq:=DAC_SPEED[Form1.cb_spi_speed.ItemIndex+1];
  if (Form1.rg_dac_load.ItemIndex = 0) then x:=2 else x:=1;
  if ((DAC_DeviceCurrent = DAC_1273HA015_NUM) OR (DAC_DeviceCurrent = DAC_1273HA025_NUM)) then x:=1;

  if ( DAC_Data.count0> 0) then
  begin
//    Form1.L_period0.Caption:= Get_period(DAC_Data.count0*DAC_SPI_BIT_COUNT/(StrToFloat(a)*1000000));
//    Form1.L_frieq0.Caption:=  Get_frieq(StrToFloat(a)*1000000/(DAC_Data.count0*DAC_SPI_BIT_COUNT));
    Form1.L_period0.Caption:= Get_period(DAC_Data.count0*x/frieq);
    Form1.L_frieq0.Caption:=  Get_frieq(frieq/(DAC_Data.count0*x));

  end;
  if ( DAC_Data.count1> 0) then
  begin
//    Form1.L_period1.Caption:= Get_period(DAC_Data.count1*DAC_SPI_BIT_COUNT/(StrToFloat(a)*1000000));
//    Form1.L_frieq1.Caption:=  Get_frieq(StrToFloat(a)*1000000/(DAC_Data.count1*DAC_SPI_BIT_COUNT));
    Form1.L_period1.Caption:= Get_period(DAC_Data.count1*x/frieq);
    Form1.L_frieq1.Caption:=  Get_frieq(frieq/(DAC_Data.count1*x));
  end;
end;

procedure DAC_Read_All_Regs();
var i:byte;
begin
{  for i := 0 to 3 do
  begin
    WriteStringComm(DAC_REG_READ+normalize_addr(DEC2HEX(i*4),1)+#13);Wait_Rx();
  end;}
  for i := 0 to 13 do
  begin
    WriteStringComm(DAC_REG_READB+normalize_addr(DEC2HEX(i),1)+#13);Wait_Rx();
  end;
end;

Function MyReadWave(FileName : AnsiString) : TWaveResult;
var
  f             : TFileStream;
  fin           : File;
  wFileSize     : Cardinal;
  wChankSize    : Cardinal;
  ID            : array[0..3] of Char;
  Header        : TWaveHeaderChank;
  RealFileSize  : Cardinal;
  buff          : array of Byte;
Begin
  FillChar(Result, SizeOf(Result), 0);

  Try
    f := TFileStream.Create(FileName, fmOpenRead);
    f.Seek(0, soFromBeginning);
    f.Read(buff,f.Size);
//    f.Seek(0, 0);
//    f.ReadBuffer(ID[0], 4);       //������ ��� �����
    Form1.memo1.Lines.Add('FileSize ' + intToStr(f.Size));
    f.Read(ID[0], 4);
//    AssignFile(fin,FileName);

    if String(ID) <> 'RIFF'       //���������� ��� �����
    then
      Begin
        Result.ERROR := IncorectFileFormat;
        Form1.memo1.Lines.Add('IncorectFileFormat: '+String(ID));
        f.Free;
        exit;
      end;
      //////////////////////////
      Form1.memo1.Lines.Add(String(ID));

    f.ReadBuffer(wFileSize, 4);   //������ ������ �����
      //////////////////////////
      Form1.memo1.Lines.Add('FileSize ' + intToStr(wFileSize));

    if f.size <> (wFileSize + 8)  //���������� ������������ ���������� �������
    then                          //� ������� �����(�� ������ ���� ��� ��������� ���
      Begin                       //��������)
        Result.ERROR := FileCorrupt;
        f.Free;
        exit;
      end;

    f.ReadBuffer(ID[0], 4);
      //////////////////////////
      Form1.memo1.Lines.Add(String(ID));

    if String(ID) <> 'WAVE'             //���������� ������ �����
    then
      Begin
        Result.ERROR := IncorectFileFormat;
        f.Free;
        exit;
      end;

    wChankSize := 0;
    repeat                              //���� ���� �������
      f.Seek(wChankSize, soFromCurrent);//���������� ��� �������������� �����
      f.ReadBuffer(ID[0], 4);           //������ ������������� �����

        //////////////////////////
        Form1.memo1.Lines.Add(String(ID));
      f.ReadBuffer(wChankSize, 4);   //������ ������ �����

      if wChankSize > High(integer)  //��������� ������ �������� �� ����������
      then                           //������� �������� ����� ���������� ���� �
        Begin                        //������ �������� 100
          Result.ERROR := DataError;
          f.Free;
          exit;
        end;
        //////////////////////////
        Form1.memo1.Lines.Add('chankSize ' + intToStr(wChankSize));
    until  (String(ID)='fmt ') or (String(ID)='data');

    if String(ID)='data'             //��������� ������ �� ��������� �������
    then
      Begin
        Result.ERROR := HeaderError;
        f.Free;
        exit;
      end;

    f.ReadBuffer(Header, Min(wChankSize, SizeOf(TWaveHeaderChank))); //������ ���������
      //////////////////////////               //������ ����� ���������
      Form1.memo1.Lines.Add('wFormatTag '     + intToStr(Header.wFormatTag));
      Form1.memo1.Lines.Add('wChannels '      + intToStr(Header.wChannels));
      Form1.memo1.Lines.Add('wSamplesPerSec ' + intToStr(Header.wSamplesPerSec));
      Form1.memo1.Lines.Add('wBlockAlign '    + intToStr(Header.wBlockAlign));
      Form1.memo1.Lines.Add('wBitsPerSample ' + intToStr(Header.wBitsPerSample));

    if wChankSize > SizeOf(TWaveHeaderChank)  //������� ��������� ������ � ����� �����
    then                                      //����� ������ ��� ������� ����������
      f.Seek(wChankSize - SizeOf(TWaveHeaderChank), soFromCurrent);

    if wChankSize >= SizeOf(TWaveHeaderChank) //���������� ����������� �� ��������� ��� ���
    then
      //////////////////////////
        Form1.memo1.Lines.Add('wcbSize '        + intToStr(Header.wcbSize));

    wChankSize := 0;
    repeat                              //���� ���� ������
      f.Seek(wChankSize, soFromCurrent);//���������� ��� �������������� �����
      f.ReadBuffer(ID[0], 4);           //������ ������������� �����
        //////////////////////////
        Form1.memo1.Lines.Add(String(ID));
      f.ReadBuffer(wChankSize, 4);      //������  ������ �����
        //////////////////////////
        Form1.memo1.Lines.Add('chankSize '    + intToStr(wChankSize));
    until  String(ID)='data';

    Result.ERROR           := noError;               //��������� ��������� ����������
    Result.wAvgBytesPerSec := Header.wAvgBytesPerSec;
    Result.wBitsPerSample  := Header.wBitsPerSample;
    Result.wChannels       := Header.wChannels;
                                                    //�������� ������ � ������
    Result.Data := TMemoryStream.Create;
    Result.Data.Seek(0, soFromBeginning);
    Result.Data.Size := wChankSize;                 //�������� ������ ��� ������

    f.ReadBuffer(Result.Data.Memory^, wChankSize);  //�������� ������ � ������

  Except
    Result.ERROR := ReadError;
  end;
  f.Free;
End;

Function ReadWave(FileName : AnsiString) : TWaveResult;
var
  f             : TFileStream;
  fin           : File;
  wFileSize     : Cardinal;
  wChankSize    : Cardinal;
  ID            : array[0..3] of Char;
  Header        : TWaveHeaderChank;
  RealFileSize  : Cardinal;

Begin
  FillChar(Result, SizeOf(Result), 0);

  Try
    f := TFileStream.Create(FileName, fmOpenRead);
    f.Seek(0, soFromBeginning);
//    f.Seek(0, 0);
//    f.ReadBuffer(ID[0], 4);       //������ ��� �����
    Form1.memo1.Lines.Add('FileSize ' + intToStr(f.Size));
    f.Read(ID[0], 4);
//    AssignFile(fin,FileName);

    if String(ID) <> 'RIFF'       //���������� ��� �����
    then
      Begin
        Result.ERROR := IncorectFileFormat;
        Form1.memo1.Lines.Add('IncorectFileFormat: '+String(ID));
        f.Free;
        exit;
      end;
      //////////////////////////
      Form1.memo1.Lines.Add(String(ID));

    f.ReadBuffer(wFileSize, 4);   //������ ������ �����
      //////////////////////////
      Form1.memo1.Lines.Add('FileSize ' + intToStr(wFileSize));

    if f.size <> (wFileSize + 8)  //���������� ������������ ���������� �������
    then                          //� ������� �����(�� ������ ���� ��� ��������� ���
      Begin                       //��������)
        Result.ERROR := FileCorrupt;
        f.Free;
        exit;
      end;

    f.ReadBuffer(ID[0], 4);
      //////////////////////////
      Form1.memo1.Lines.Add(String(ID));

    if String(ID) <> 'WAVE'             //���������� ������ �����
    then
      Begin
        Result.ERROR := IncorectFileFormat;
        f.Free;
        exit;
      end;

    wChankSize := 0;
    repeat                              //���� ���� �������
      f.Seek(wChankSize, soFromCurrent);//���������� ��� �������������� �����
      f.ReadBuffer(ID[0], 4);           //������ ������������� �����

        //////////////////////////
        Form1.memo1.Lines.Add(String(ID));
      f.ReadBuffer(wChankSize, 4);   //������ ������ �����

      if wChankSize > High(integer)  //��������� ������ �������� �� ����������
      then                           //������� �������� ����� ���������� ���� �
        Begin                        //������ �������� 100
          Result.ERROR := DataError;
          f.Free;
          exit;
        end;
        //////////////////////////
        Form1.memo1.Lines.Add('chankSize ' + intToStr(wChankSize));
    until  (String(ID)='fmt ') or (String(ID)='data');

    if String(ID)='data'             //��������� ������ �� ��������� �������
    then
      Begin
        Result.ERROR := HeaderError;
        f.Free;
        exit;
      end;

    f.ReadBuffer(Header, Min(wChankSize, SizeOf(TWaveHeaderChank))); //������ ���������
      //////////////////////////               //������ ����� ���������
      Form1.memo1.Lines.Add('wFormatTag '     + intToStr(Header.wFormatTag));
      Form1.memo1.Lines.Add('wChannels '      + intToStr(Header.wChannels));
      Form1.memo1.Lines.Add('wSamplesPerSec ' + intToStr(Header.wSamplesPerSec));
      Form1.memo1.Lines.Add('wBlockAlign '    + intToStr(Header.wBlockAlign));
      Form1.memo1.Lines.Add('wBitsPerSample ' + intToStr(Header.wBitsPerSample));

    if wChankSize > SizeOf(TWaveHeaderChank)  //������� ��������� ������ � ����� �����
    then                                      //����� ������ ��� ������� ����������
      f.Seek(wChankSize - SizeOf(TWaveHeaderChank), soFromCurrent);

    if wChankSize >= SizeOf(TWaveHeaderChank) //���������� ����������� �� ��������� ��� ���
    then
      //////////////////////////
        Form1.memo1.Lines.Add('wcbSize '        + intToStr(Header.wcbSize));

    wChankSize := 0;
    repeat                              //���� ���� ������
      f.Seek(wChankSize, soFromCurrent);//���������� ��� �������������� �����
      f.ReadBuffer(ID[0], 4);           //������ ������������� �����
        //////////////////////////
        Form1.memo1.Lines.Add(String(ID));
      f.ReadBuffer(wChankSize, 4);      //������  ������ �����
        //////////////////////////
        Form1.memo1.Lines.Add('chankSize '    + intToStr(wChankSize));
    until  String(ID)='data';

    Result.ERROR           := noError;               //��������� ��������� ����������
    Result.wAvgBytesPerSec := Header.wAvgBytesPerSec;
    Result.wBitsPerSample  := Header.wBitsPerSample;
    Result.wChannels       := Header.wChannels;
                                                    //�������� ������ � ������
    Result.Data := TMemoryStream.Create;
    Result.Data.Seek(0, soFromBeginning);
    Result.Data.Size := wChankSize;                 //�������� ������ ��� ������

    f.ReadBuffer(Result.Data.Memory^, wChankSize);  //�������� ������ � ������

  Except
    Result.ERROR := ReadError;
  end;
  f.Free;
end;

procedure ReadWaveHeader(Stream: TStream;
  var SampleCount, SamplesPerSec: integer;
  var BitsPerSample, Channeles: smallint);
var
  WaveHeader: TWaveHeader;
  DataHeader: TDataHeader;
begin

  Stream.Read(WaveHeader, sizeof(TWaveHeader));
  Form1.Memo1.Lines.Add('Open Stream: Read Wave Header ');
  with WaveHeader do
  begin
    Form1.Memo1.Lines.Add('  Wave Header: idWave '+idWave);
    if idRiff <> 'RIFF' then
      raise EReadError.Create('Wrong idRIFF');
    if idWave <> 'WAVE' then
      raise EReadError.Create('Wrong idWAVE');
    if idFmt <> 'fmt ' then
      raise EReadError.Create('Wrong idFmt');
    if WaveType <> 1 then
      raise EReadError.Create('Unknown format');

    Channeles := Ch;
    SamplesPerSec := Freq;
    BitsPerSample := Bits;
    Stream.Seek(InfoLen - 16, soFromCurrent);
  end;
  Stream.Read(DataHeader, sizeof(TDataHeader));
  Form1.Memo1.Lines.Add('Open Stream: Read Data Header ');
  if DataHeader.idData = 'fact' then
  begin
    Stream.Seek(4, soFromCurrent);
    Stream.Read(DataHeader, sizeof(TDataHeader));
  end;
  with DataHeader do
  begin
    if idData <> 'data' then
      raise EReadError.Create('Wrong idData');
    SampleCount := DataLen div (Channeles * BitsPerSample div 8)
  end;
end;

procedure Graph_update();
var i:integer;
begin
  Form1.Chart1.Series[0].Clear;
  Form1.Lpointcnt0.Caption:=inttostr(DAC_Data.count0);
  if (DAC_Data.repeat0 > 1) then for i := 1 to DAC_Data.count1 do Form1.Chart1.Series[0].AddXY(i,DAC_Data.data[i].ch0,'',clRed)
    else  for i := 1 to DAC_Data.count0 do Form1.Chart1.Series[0].AddXY(i,DAC_Data.data[i].ch0,'',clRed);
  Form1.Chart1.Series[1].Clear;
  Form1.Lpointcnt1.Caption:=inttostr(DAC_Data.count1);
  if (DAC_Data.repeat1 > 1) then for i := 1 to DAC_Data.count0 do Form1.Chart1.Series[1].AddXY(i,DAC_Data.data[i].ch1,'',clRed)
    else for i := 1 to DAC_Data.count1 do Form1.Chart1.Series[1].AddXY(i,DAC_Data.data[i].ch1,'',clRed);
end;

function MaxPointValid(count:integer):boolean;
var general_count:integer;
begin
  if ((DAC_DeviceCurrent = DAC_1273HA015_NUM) OR (DAC_DeviceCurrent = DAC_1273HA025_NUM) OR (Form1.rg_dac_load.ItemIndex = 1)) then general_count:=count
    else general_count:=count*2;
  if (general_count <= DAC_MaxPoint) then Result:=True else Result:=False;
  if (Not Result) then MessageDlg('��������� ������������ ���������� ����� ('+inttostr(DAC_MaxPoint)+' �����)',mtError, mbOKCancel, 0);
end;

procedure CalcPoint_sin(ch:byte; PointCnt:Integer;min,max:Int64);
const SHAPE_NAME='SIN';
      SHAPE_INDEX=0;
var offset:Int64;
    amplitude:integer;
    sin_val:real;
    i,count:integer;
begin
    amplitude:=trunc((max-min)*0.5);
    offset:=min+trunc((max-min)*0.5);
//  amplitude:= 16384;
  if (ch = 2) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.count1:=pointcnt;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min0:=min;  DAC_Data.min1:=min;
    DAC_Data.max0:=max;  DAC_Data.max1:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    for i := 1 to pointcnt do
    begin
      DAC_Data.data[i].ch0:=offset+trunc(sin(i*2*PI/pointcnt)*amplitude);
      DAC_Data.data[i].ch1:=DAC_Data.data[i].ch0;
    end;
  end;
  if (ch = 0) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.min0:=min;
    DAC_Data.max0:=max;
    DAC_Data.repeat0:=0;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count1>DAC_Data.count0) then
    begin
      DAC_Data.repeat0:=trunc(DAC_Data.count1/DAC_Data.count0);
      count:=0;
      while(count<DAC_Data.count1) do
        for i := 1 to pointcnt do
        begin
          inc(count);
          DAC_Data.data[count].ch0:=offset+trunc(sin(i*2*PI/pointcnt)*amplitude);
        end;
    end else
    begin
      for i := 1 to pointcnt do DAC_Data.data[i].ch0:=offset+trunc(sin(i*2*PI/pointcnt)*amplitude);
    end;
  end;
  if (ch = 1) then
  begin
    DAC_Data.count1:=pointcnt;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min1:=min;
    DAC_Data.max1:=max;
    DAC_Data.repeat1:=0;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count0>DAC_Data.count1) then
    begin
      DAC_Data.repeat1:=trunc(DAC_Data.count0/DAC_Data.count1);
      count:=0;
      while(count<DAC_Data.count0) do
        for i := 1 to pointcnt do begin inc(count); DAC_Data.data[count].ch1:=offset+trunc(sin(i*2*PI/pointcnt)*amplitude); end;
    end else
    begin
      for i := 1 to pointcnt do DAC_Data.data[i].ch1:=offset+trunc(sin(i*2*PI/pointcnt)*amplitude);
    end;
  end;
end;

procedure Gen_sin(ch:byte);
const SHAPE_NAME='SIN';
      SHAPE_INDEX=0;
var pointcnt:integer;
    max,min:Int64;
    data:Int64;
    s:string;
begin
  if (PintCountLast > 0) then s:=inttostr(PintCountLast)
    else  s:=inttostr(PintCount_Default);
  pointcnt:=strtoint(InputBox('������ ��������������� �������','���������� �����',s));
 if (MaxPointValid(pointcnt)) then
 begin
  PintCountLast:=pointcnt;
  if (Form1.N_full.Checked) then
  begin
    max:=trunc(power(2,DAC_MAX_DIGIT))-2;
    min:=1;
  end else
  begin
    data:=strtoint(InputBox('��������� �����','������ �������','0'));
    if (data < trunc(power(2,DAC_MAX_DIGIT))) then min:=data else min:=0;
    data:=strtoint(InputBox('��������� �����','������� �������',inttostr(trunc(power(2,DAC_MAX_DIGIT))-1)));
    if (data <= trunc(power(2,DAC_MAX_DIGIT))) then max:=data else max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  end;
  CalcPoint_sin(ch,pointcnt,min,max);
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
 end;
end;

procedure CalcPoint_meandr(ch:byte; PointCnt:Integer;min,max:Int64);
const SHAPE_NAME='MEANDR';
      SHAPE_INDEX=1;
var offset:Int64;
    amplitude:integer;
    i,count:integer;
begin
    amplitude:=trunc((max-min)*0.5);
    offset:=min+trunc((max-min)*0.5);
//  max:=trunc(power(2,DAC_MAX_DIGIT))-2;
//  amplitude:=trunc(max*0.5);
//  amplitude:= 16384;
  if (ch = 2) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.count1:=pointcnt;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min0:=min;  DAC_Data.min1:=min;
    DAC_Data.max0:=max;  DAC_Data.max1:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    for i := 1 to pointcnt do
    begin
      if (i<=pointcnt/2) then
      begin
        DAC_Data.data[i].ch0:=max;
        DAC_Data.data[i].ch1:=min;
      end else
      begin
        DAC_Data.data[i].ch0:=min;
        DAC_Data.data[i].ch1:=max;
      end;
    end;
  end;
  if (ch = 0) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.repeat0:=0;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.min0:=min;
    DAC_Data.max0:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count1>DAC_Data.count0) then
    begin
      DAC_Data.repeat0:=trunc(DAC_Data.count1/DAC_Data.count0);
      count:=0;
      while(count<DAC_Data.count1) do
        for i := 1 to pointcnt do begin
         inc(count);
         if (i<=pointcnt/2) then DAC_Data.data[count].ch0:=max
           else DAC_Data.data[count].ch0:=min;
           end;
    end else
    begin
    for i := 1 to pointcnt do
      if (i<=pointcnt/2) then DAC_Data.data[i].ch0:=max
        else DAC_Data.data[i].ch0:=min;
    end;
  end;
  if (ch = 1) then
  begin
    DAC_Data.count1:=pointcnt;
    DAC_Data.repeat1:=0;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min1:=min;
    DAC_Data.max1:=max;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count0>DAC_Data.count1) then
    begin
      DAC_Data.repeat1:=trunc(DAC_Data.count0/DAC_Data.count1);
      count:=0;
      while(count<DAC_Data.count0) do
        for i := 1 to pointcnt do begin
         inc(count);
         if (i<=pointcnt/2) then DAC_Data.data[count].ch1:=max
           else DAC_Data.data[count].ch1:=min;
           end;
    end else
    begin
    for i := 1 to pointcnt do
      if (i<=pointcnt/2) then DAC_Data.data[i].ch1:=min
        else DAC_Data.data[i].ch1:=max;
    end;
  end;
end;

procedure Gen_meandr(ch:byte);
const SHAPE_NAME='MEANDR';
      SHAPE_INDEX=1;
var min,max,offset:Int64;
    amplitude:integer;
    pointcnt:integer;
    data:Int64;
    s:string;
begin
  if (PintCountLast > 0) then s:=inttostr(PintCountLast)
    else  s:=inttostr(PintCount_Default);
  pointcnt:=strtoint(InputBox('������ ������� "������"','���������� �����',s));
  PintCountLast:=pointcnt;
  if (Form1.N_full.Checked) then
  begin
    max:=trunc(power(2,DAC_MAX_DIGIT))-2;
    min:=1;
  end else
  begin
    data:=strtoint(InputBox('��������� �����','������ �������','0'));
    if (data < trunc(power(2,DAC_MAX_DIGIT))) then min:=data else min:=0;
    data:=strtoint(InputBox('��������� �����','������� �������',inttostr(trunc(power(2,DAC_MAX_DIGIT))-1)));
    if (data <= trunc(power(2,DAC_MAX_DIGIT))) then max:=data else max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  end;
  CalcPoint_meandr(ch,pointcnt,min,max);
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
end;

procedure CalcPoint_rise(ch:byte; PointCnt:Integer;min,max:Int64);
const SHAPE_NAME='SAW';
      SHAPE_INDEX=2;
var offset:Int64;
    amplitude:integer;
    i,count:integer;
    val,step:real;
begin
    amplitude:=trunc((max-min)*0.5);
    offset:=trunc((max-min)*0.5);
//  max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  step:=(max-min)/pointcnt;
 // amplitude:=trunc(max*0.5);
  val:=min;
//  amplitude:= 16384;
  if (ch = 2) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.count1:=pointcnt;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min0:=min;  DAC_Data.min1:=min;
    DAC_Data.max0:=max;  DAC_Data.max1:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    for i := 1 to pointcnt do
    begin
      DAC_Data.data[i].ch0:=trunc(val);
      DAC_Data.data[i].ch1:=DAC_Data.data[i].ch0;
      val:=val+step;
    end;
  end;
  if (ch = 0) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.repeat0:=0;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.min0:=min;
    DAC_Data.max0:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count1>DAC_Data.count0) then
    begin
      DAC_Data.repeat0:=trunc(DAC_Data.count1/DAC_Data.count0);
      count:=0;
      while(count<DAC_Data.count1) do
      begin
        val:=min;
        for i := 1 to pointcnt do
        begin
          inc(count);
          DAC_Data.data[count].ch0:=trunc(val);
          val:=val+step;
        end;
      end;
    end else
    begin
      for i := 1 to pointcnt do
      begin
        DAC_Data.data[i].ch0:=trunc(val);
        val:=val+step;
      end;
    end;
  end;
  if (ch = 1) then
  begin
    DAC_Data.count1:=pointcnt;
    DAC_Data.repeat1:=0;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min1:=min;
    DAC_Data.max1:=max;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count0>DAC_Data.count1) then
    begin
      DAC_Data.repeat1:=trunc(DAC_Data.count0/DAC_Data.count1);
      count:=0;
      while(count<DAC_Data.count0) do
      begin
        val:=min;
        for i := 1 to pointcnt do
        begin
          inc(count);
          DAC_Data.data[count].ch1:=trunc(val);
          val:=val+step;
        end;
      end;
    end else
    begin
      for i := 1 to pointcnt do
      begin
        DAC_Data.data[i].ch1:=trunc(val);
        val:=val+step;
      end;
    end;
  end;
end;

procedure Gen_rise(ch:byte);
const SHAPE_NAME='SAW';
      SHAPE_INDEX=2;
var min,max,offset:Int64;
    amplitude:integer;
    pointcnt:integer;
    i,count:integer;
    data:Int64;
    s:string;

begin
  if (PintCountLast > 0) then s:=inttostr(PintCountLast)
    else  s:=inttostr(PintCount_Default);
  pointcnt:=strtoint(InputBox('������ ������� "Rise"','���������� �����',s));
  PintCountLast:=pointcnt;
 if (MaxPointValid(pointcnt)) then
 begin
  if (Form1.N_full.Checked) then
  begin
    max:=trunc(power(2,DAC_MAX_DIGIT))-2;
    min:=1;
  end else
  begin
    data:=strtoint(InputBox('��������� �����','������ �������','0'));
    if (data < trunc(power(2,DAC_MAX_DIGIT))-1) then min:=data else min:=0;
    data:=strtoint(InputBox('��������� �����','������� �������',inttostr(trunc(power(2,DAC_MAX_DIGIT))-1)));
    if (data < trunc(power(2,DAC_MAX_DIGIT))) then max:=data else max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  end;
  CalcPoint_rise(ch,pointcnt,min,max);
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
 end;
end;

procedure CalcPoint_triangle(ch:byte; PointCnt:Integer;min,max:Int64);
const SHAPE_NAME='TRIANGLE';
      SHAPE_INDEX=3;
var offset:Int64;
    amplitude:integer;
    i,count:integer;
    val,step:real;
begin
    amplitude:=trunc((max-min)*0.5);
    offset:=trunc((max-min)*0.5);
//  max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  step:=(max-min)*2/pointcnt;
//  amplitude:=trunc(max*0.5);
  val:=min;
//  amplitude:= 16384;
  if (ch = 2) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.count1:=pointcnt;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min0:=min;  DAC_Data.min1:=min;
    DAC_Data.max0:=max;  DAC_Data.max1:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    for i := 1 to pointcnt do
    begin
      DAC_Data.data[i].ch0:=trunc(val);
      DAC_Data.data[i].ch1:=DAC_Data.data[i].ch0;
      if (i<=pointcnt*0.5) then val:=val+step else val:=val-step;
    end;
  end;
  if (ch = 0) then
  begin
    DAC_Data.count0:=pointcnt;
    DAC_Data.repeat0:=0;
    DAC_Data.shape0:=SHAPE_NAME;
    DAC_Data.min0:=min;
    DAC_Data.max0:=max;
//    Form1.L_shapes_ch0.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count1>DAC_Data.count0) then
    begin
      DAC_Data.repeat0:=trunc(DAC_Data.count1/DAC_Data.count0);
      count:=0;
      while(count<DAC_Data.count1) do
      begin
        val:=min;
        for i := 1 to pointcnt do begin
         inc(count);
          DAC_Data.data[count].ch0:=trunc(val);
          if (i<=pointcnt*0.5) then val:=val+step else val:=val-step;
           end;
      end;
    end else
    begin
      for i := 1 to pointcnt do
      begin
        DAC_Data.data[i].ch0:=trunc(val);
        if (i<=pointcnt*0.5) then val:=val+step else val:=val-step;
      end;
    end;
  end;
  if (ch = 1) then
  begin
    DAC_Data.count1:=pointcnt;
    DAC_Data.repeat1:=0;
    DAC_Data.shape1:=SHAPE_NAME;
    DAC_Data.min1:=min;
    DAC_Data.max1:=max;
//    Form1.L_shapes_ch1.Caption:=SHAPE_NAME;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    if(DAC_Data.count0>DAC_Data.count1) then
    begin
      DAC_Data.repeat1:=trunc(DAC_Data.count0/DAC_Data.count1);
      count:=0;
      while(count<DAC_Data.count0) do
      begin
        val:=min;
        for i := 1 to pointcnt do begin
         inc(count);
          DAC_Data.data[count].ch1:=trunc(val);
          if (i<=pointcnt*0.5) then val:=val+step else val:=val-step;
           end;
      end;
    end else
    begin
      for i := 1 to pointcnt do
      begin
        DAC_Data.data[i].ch1:=trunc(val);
        if (i<=pointcnt*0.5) then val:=val+step else val:=val-step;
      end;
    end;
  end;
end;

procedure Gen_triangle(ch:byte);
const SHAPE_NAME='TRIANGLE';
      SHAPE_INDEX=3;
var min,max,offset:Int64;
    amplitude:integer;
    pointcnt:integer;
    i,count:integer;
    data:Int64;
    s:string;
    val,step:real;
begin
  if (PintCountLast > 0) then s:=inttostr(PintCountLast)
    else  s:=inttostr(PintCount_Default);
  pointcnt:=strtoint(InputBox('������ ������� "�����������"','���������� �����',s));
 if (MaxPointValid(pointcnt)) then
 begin
  PintCountLast:=pointcnt;
  if (Form1.N_full.Checked) then
  begin
    max:=trunc(power(2,DAC_MAX_DIGIT))-2;
    min:=1;
  end else
  begin
    data:=strtoint(InputBox('��������� �����','������ �������','0'));
    if (data < trunc(power(2,DAC_MAX_DIGIT))) then min:=data else min:=0;
    data:=strtoint(InputBox('��������� �����','������� �������',inttostr(trunc(power(2,DAC_MAX_DIGIT))-1)));
    if (data <= trunc(power(2,DAC_MAX_DIGIT))) then max:=data else max:=trunc(power(2,DAC_MAX_DIGIT))-2;
  end;

  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
 end;
end;

procedure Gen_String(ch:byte);
const SHAPE_NAME='STRING';
      SHAPE_INDEX=4;
var min,max,offset:Int64;
var str,s:string;
    i,pointcnt:integer;
begin
  pointcnt:=0;
  if (ch = 2) then s:=' ������� � � �'
    else if (ch = 0) then s:=' ������ �'
    else s:=' ������ B';
  str:=InputBox('���� ����� ','�������� ����� (����������� - ",")','0,2,4,6');
  if (ch = 2) then
  begin
//    Form1.L_shapes_ch0.Caption:='STRING';
//    Form1.L_shapes_ch1.Caption:='STRING';
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    while (pos(',',str)>0) do
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch0:=strtoint(copy(str,1,pos(',',str)-1));
      DAC_Data.data[pointcnt].ch1:=DAC_Data.data[pointcnt].ch0;
      delete(str,1,pos(',',str));
    end;
    if (length(str)>0) then
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch0:=strtoint(str);
      DAC_Data.data[pointcnt].ch1:=DAC_Data.data[pointcnt].ch0;
    end;
    DAC_Data.count0:=pointcnt;
    DAC_Data.count1:=pointcnt;
  end;

  if (ch = 0) then
  begin
//    Form1.L_shapes_ch0.Caption:='STRING';
    Form1.cb_shapes_ch0.ItemIndex:=SHAPE_INDEX;
    while (pos(',',str)>0) do
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch0:=strtoint(copy(str,1,pos(',',str)-1));
      delete(str,1,pos(',',str));
    end;
    if (length(str)>0) then
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch0:=strtoint(str);
    end;
    DAC_Data.count0:=pointcnt;
  end;

  if (ch = 1) then
  begin
//    Form1.L_shapes_ch1.Caption:='STRING';
    Form1.cb_shapes_ch1.ItemIndex:=SHAPE_INDEX;
    while (pos(',',str)>0) do
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch1:=strtoint(copy(str,1,pos(',',str)-1));
      delete(str,1,pos(',',str));
    end;
    if (length(str)>0) then
    begin
      inc(pointcnt);
      DAC_Data.data[pointcnt].ch1:=strtoint(str);
    end;
    DAC_Data.count1:=pointcnt;
  end;
 if (MaxPointValid(pointcnt)) then
 begin
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
 end;
end;


procedure DAC_Buffer_Write(datacount:integer);
const MAX_BUFF_ADD = 30;
var i,j,npoint:integer;
    s:string;
    Dac015_PowerSave:Int64;
begin
  Dac015_PowerSave:=Form1.cb_015_out.ItemIndex shl 16;
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write (Start) ');
      Add_log('<DBG>: Func DAC_Buffer_Write (datacount= '+inttostr(datacount)+') ');
      Add_log('<DBG>: Func DAC_Buffer_Write (DAC_DeviceCurrent= '+inttostr(DAC_DeviceCurrent)+') ');
    {$EndIf}
  if ((DAC_DeviceCurrent = DAC_1273HA015_NUM) OR (DAC_DeviceCurrent = DAC_1273HA025_NUM) OR (Form1.rg_dac_load.ItemIndex = 1)) then
  begin
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write: (1273HA015 | DAC_1273HA025 | Parallel)');
    {$EndIf}
    i:=1;
    while ((i<=datacount) AND (Not RxBuff_timeout) AND (NOT BufferOverFlow)) do
    begin
      s:=DAC_BUFF_ADDN;
      if ((datacount - i+1) > MAX_BUFF_ADD) then j:= MAX_BUFF_ADD
        else j:= datacount - i+1;
      s:=s+DEC2HEX(j);
      npoint:=j;
      while(j>0) do
      begin
        if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then s:=s+' 3'+ DEC2HEX(DAC_Data.data[i].ch0)
        else if (DAC_DeviceCurrent = DAC_1273HA015_NUM) then s:=s+' '+ DEC2HEX(DAC_Data.data[i].ch0+Dac015_PowerSave)
          else s:=s+' '+ DEC2HEX(DAC_Data.data[i].ch0);
        inc(i);
        dec(j);
      end;
      WriteStringComm(s+' '+#13);Wait_Rx();
      Add_log('Packege Load '+inttostr(npoint)+' point.');
    end;
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write: (1273HA015 | DAC_1273HA025 | Parallel) -> End Load');
    {$EndIf}
  end else
  if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then
  begin
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write (DAC_1273HA034) ');
    {$EndIf}
    if (Form1.rg_dac_load.ItemIndex = 0) then
    begin
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write (DAC_1273HA034): Prepare Data ');
    {$EndIf}
      i:=1;
      while ((i<=datacount) AND (Not RxBuff_timeout) AND (NOT BufferOverFlow))  do
      begin
        s:=DAC_BUFF_ADDN;
        if ((datacount - i+1) > MAX_BUFF_ADD shr 1) then j:= MAX_BUFF_ADD shr 1
          else j:= datacount - i+1;
        s:=s+DEC2HEX(j*2);
        npoint:=j*2;
        while(j>0) do
        begin
          s:=s+' 1'+normalize_addr(DEC2HEX(DAC_Data.data[i].ch0),2)+' 2'+normalize_addr(DEC2HEX(DAC_Data.data[i].ch1),2);
          inc(i);
          dec(j);
        end;
//      Add_log('-- Write: '+s+' ');
        WriteStringComm(s+' '+#13);Wait_Rx();
        Add_log('Packege Load '+inttostr(npoint)+' point.');
      end;
    end;
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write (DAC_1273HA034) End');
    {$EndIf}
  end;
    {$IfDef DBGMODE}
      Add_log('<DBG>: Func DAC_Buffer_Write (End) ');
    {$EndIf}
end;

procedure DAC1273NA_Write_Data(divider:byte);
var data_cnt,i:Word;
    s:string;
    iCounterPerSec: TLargeInteger;
    C1, C2: TLargeInteger;
    crc_calc:Cardinal;
    Rx_count_last:integer;
    TimeOut:integer;
begin
  QueryPerformanceFrequency(iCounterPerSec);
  QueryPerformanceCounter(C1);
  BufferOverFlow:=False;
//  Form1.StatusBar1.Panels[2].Text:= s;
  if (BufferRunning) then
  begin
    Form1.StatusBar1.Panels[2].Text:='... Stop Buffer ...';
    WriteStringComm(DAC_BUFF_STOP+#13);Wait_Rx();
    Form1.Btn_dac_start.Enabled:=True;
    Form1.Btn_dac_stop.Enabled:=False;
  end;
  Form1.StatusBar1.Panels[2].Text:='... Load Point to Buffer ...';
  WriteStringComm(SPI_SET_SPEED+inttostr(trunc(strtofloat(StringReplace(Form1.cb_spi_speed.Text, '.', ',',[rfReplaceAll]))*1000000))+#13);Wait_Rx();
  if (DAC_Data.count0 > DAC_Data.count1) then data_cnt:=DAC_Data.count0 else data_cnt:=DAC_Data.count1;

  if (DAC_DeviceCurrent = DAC_1273HA034_NUM) then
  begin
    if (Form1.rg_dac_load.ItemIndex = 1) then
    begin
      {$IfDef DBGMODE}
        Add_log('<DBG>: Select DAC_1273HA034: MODE_PARALLEL');
      {$EndIf}
      WriteStringComm(DAC_MODE_PARALLEL+'01'+#13);Wait_Rx();
      WriteStringComm(DAC_BUFF_SIZE+DEC2HEX(data_cnt)+#13);Wait_Rx();
      if (DAC_Buff_Pack) then DAC_Buffer_Write(data_cnt)
      else
      for i := 1 to data_cnt do
      begin
        s:='03'+normalize_addr(DEC2HEX(DAC_Data.data[i].ch0),2);
        if (Not RxBuff_timeout) then begin WriteStringComm(DAC_BUFF_ADD+s+#13);Wait_Rx(); end;
      end;

    end else
    begin
      {$IfDef DBGMODE}
        Add_log('<DBG>: Select DAC_1273HA034: NO MODE_PARALLEL');
      {$EndIf}
      WriteStringComm(DAC_MODE_PARALLEL+'00'+#13);Wait_Rx();
      if (Form1.cb_dac_sync.Checked) then begin WriteStringComm(DAC_UPDATE_SYNC+'01'+#13);Wait_Rx(); end
        else begin WriteStringComm(DAC_UPDATE_SYNC+'00'+#13);Wait_Rx(); end;
      WriteStringComm(DAC_BUFF_SIZE+DEC2HEX(data_cnt*2)+#13);Wait_Rx();
      if (DAC_Buff_Pack) then DAC_Buffer_Write(data_cnt)
        else for i := 1 to data_cnt do
          begin
            s:='01'+normalize_addr(DEC2HEX(DAC_Data.data[i].ch0),2);
            if (Not RxBuff_timeout) then begin WriteStringComm(DAC_BUFF_ADD+s+#13);Wait_Rx(); end;
            s:='02'+normalize_addr(DEC2HEX(DAC_Data.data[i].ch1),2);
            if (Not RxBuff_timeout) then begin WriteStringComm(DAC_BUFF_ADD+s+#13);Wait_Rx(); end;
//      Add_log('Point '+inttostr(i*2-1)+', '+inttostr(i*2));
          end;
      data_cnt:= data_cnt*2;
    end;
  end
   else
  begin
    {$IfDef DBGMODE}
       Add_log('<DBG>: DAC Not DAC_1273HA034');
    {$EndIf}
    data_cnt:=DAC_Data.count0;
    WriteStringComm(DAC_BUFF_SIZE+DEC2HEX(data_cnt)+#13);Wait_Rx();
    {$IfDef DBGMODE}
       Add_log('<DBG>: FUNC DAC_Buffer_Write');
    {$EndIf}
    DAC_Buffer_Write(data_cnt);
    {  for i := 1 to data_cnt do
      begin
        s:=normalize_addr(DEC2HEX(DAC_Data.data[i].ch0),2);
        if (Not RxBuff_timeout) then begin WriteStringComm(DAC_BUFF_ADD+s+#13);Wait_Rx(); end;
      end; }
  end;
    if ((Not RxBuff_timeout) AND (NOT BufferOverFlow)) then
    begin
    {$IfDef DBGMODE}
       Add_log('<DBG>: DAC_BUFF_CRC32');
    {$EndIf}
      WriteStringComm(DAC_BUFF_CRC32+#13);Wait_Rx();
//      WriteStringComm(DAC_BUFF_DIV+DEC2HEX(divider)+#13);Wait_Rx();
//    WriteStringComm(DAC_BUFF_UPDATE+#13);Wait_Rx();
      WriteStringComm(DAC_BUFF_START+#13);Wait_Rx();

{
    Rx_count_last:=GetRxListCount();
    if (data_cnt<=50) then
    begin
      TimeOut:=65535;
      //while ((GetRxListCount() < Rx_count_last+data_cnt) AND (TimeOut>0)) do dec(TimeOut);
      for i := 0 to data_cnt-1 do
      begin
        WriteStringComm(DAC_BUFF_READ+DEC2HEX(i)+#13);Wait_Rx();
      end;
//      WriteStringComm(DAC_BUFF_ALL_READ+#13);Wait_Rx();

    end;
}

    crc_calc:=$FFFFFFFF;
    crc_calc:=GetCRC(crc_calc,@DAC_Data.data[1],data_cnt*2);
    QueryPerformanceCounter(C2);
//    s:='Load '+inttostr(data_cnt*4)+' bytes to Buffer. CRC32='+DEC2HEX(Dac_CRC32)+'h crc='+DEC2HEX(crc_calc)+'h Time: '+FormatFloat('0.0', (C2 - C1)*1000 / iCounterPerSec) + ' ����.';

  end;
    if ((RxBuff_timeout) OR (BufferOverFlow)) then
    begin
      s:=' Error TimeOut. Point not Loaded';
      if (BufferOverFlow) then
      begin
        s:=' Error OverFlow. Point not Loaded';
        MessageDlg('��������� ����������� ���������� ���������� �����',mtError, mbOKCancel, 0);
        BufferOverFlow:=False;
      end;
      Form1.Btn_dac_start.Enabled:=False;
      Form1.Btn_dac_stop.Enabled:=False;
      Form1.Btn_load_buff.Enabled:=False;
    end else s:='Load '+inttostr(data_cnt)+' Points to Buffer is end. Time: '+FormatFloat('0.0', (C2 - C1)*1000 / iCounterPerSec) + ' ����.';

    Form1.StatusBar1.Panels[2].Text:= s;
    Add_log(s);

  buff_count:=data_cnt;
  //Form1.Timer1.Enabled:=true;
end;

procedure DAC_Write_Data(divider:byte);
var data_cnt,i:Word;
    s:string;
    iCounterPerSec: TLargeInteger;
    C1, C2: TLargeInteger;
    crc_calc:Cardinal;
    Rx_count_last:integer;
    TimeOut:integer;
begin
  QueryPerformanceFrequency(iCounterPerSec);
  QueryPerformanceCounter(C1);
  if (DAC_Data.count0 > DAC_Data.count1) then data_cnt:=DAC_Data.count0 else data_cnt:=DAC_Data.count1;
  WriteStringComm(DAC_BUFF_SIZE+DEC2HEX(data_cnt)+#13);Wait_Rx();
  for i := 1 to data_cnt do
  begin
    s:=normalize_addr(DEC2HEX(DAC_Data.data[i].ch1),2);
    s:=s+normalize_addr(DEC2HEX(DAC_Data.data[i].ch0),2);
    if (Not RxBuff_timeout) then WriteStringComm(DAC_BUFF_ADD+s+#13);Wait_Rx();
  end;
  if (Not RxBuff_timeout) then
  begin
    WriteStringComm(DAC_BUFF_CRC32+#13);Wait_Rx();
    WriteStringComm(DAC_BUFF_DIV+DEC2HEX(divider)+#13);Wait_Rx();
    WriteStringComm(DAC_BUFF_UPDATE+#13);Wait_Rx();
    Rx_count_last:=GetRxListCount();
    if (data_cnt<=50) then
    begin
      TimeOut:=65535;
      //while ((GetRxListCount() < Rx_count_last+data_cnt) AND (TimeOut>0)) do dec(TimeOut);
      for i := 0 to data_cnt-1 do
      begin
        WriteStringComm(DAC_BUFF_READ+DEC2HEX(i)+#13);Wait_Rx();
      end;
//      WriteStringComm(DAC_BUFF_ALL_READ+#13);Wait_Rx();
    end;

    crc_calc:=$FFFFFFFF;
    crc_calc:=GetCRC(crc_calc,@DAC_Data.data[1],data_cnt*2);
    QueryPerformanceCounter(C2);
    Form1.StatusBar1.Panels[2].Text:='Load '+inttostr(data_cnt*4)+' bytes to FPGA Buffer. CRC32='+DEC2HEX(Dac_CRC32)+'h crc='+DEC2HEX(crc_calc)+'h Time: '+FormatFloat('0.0', (C2 - C1)*1000 / iCounterPerSec) + ' ����.';
  end;
  buff_count:=data_cnt;
  //Form1.Timer1.Enabled:=true;
end;



procedure Dac_save_file(a:string);
var f:file of byte;
    data_cnt,i:Word;
begin
  if (DAC_Data.count0 > DAC_Data.count1) then data_cnt:=DAC_Data.count0 else data_cnt:=DAC_Data.count1;
  AssignFile(f,a);
  Rewrite(f);
  BlockWrite(f,DAC_Data.data[1],data_cnt*4);
  CloseFile(f);
end;

procedure Dac_read_file(a:string);
var f:file of byte;
    data_cnt,i:Word;
    Size : Integer;
begin
  if (DAC_Data.count0 > DAC_Data.count1) then data_cnt:=DAC_Data.count0 else data_cnt:=DAC_Data.count1;
  AssignFile(f,a);
  Reset(f);
  Size := FileSize(F);
  BlockRead(f,DAC_Data.data[1],Size);
  CloseFile(f);
  DAC_Data.count0:= trunc(Size/4);
  DAC_Data.count1:= DAC_Data.count0;
end;

procedure TForm1.Btn_load_buffClick(Sender: TObject);
var data_cnt:Word;
begin
//  DAC_Write_Data(trunc(100/StrToInt(cb_dac_f.Text)));

//  DAC_Write_Data(1 shl cb_dac_f.ItemIndex);
//  DAC_DeviceCurrent:= DAC_1273HA015_NUM;
//  DAC_Buffer_Write(Dac_data.count0);
  DAC1273NA_Write_Data(1 shl cb_spi_speed.ItemIndex);
  Btn_dac_stop.Enabled:=True;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  WriteStringComm(DAC_BUFF_START+#13);Wait_Rx();
end;

procedure TForm1.cb_deviceChange(Sender: TObject);
begin
  DACSelectDev();
  Signal_calc(Form1.cb_spi_speed.Text);
end;

procedure Ch_Graph_Update(ch:byte;PointCount:integer; Min,Max:Integer);
var Index:integer;
begin
  if(ch=0) then Index:=Form1.cb_shapes_ch0.ItemIndex
    else Index:=Form1.cb_shapes_ch1.ItemIndex;
  if (Index = 0) then CalcPoint_sin(ch,PointCount,Min,Max)
    else if (Index = 1) then CalcPoint_meandr(ch,PointCount,Min,Max)
    else if (Index = 2) then CalcPoint_rise(ch,PointCount,Min,Max)
    else if (Index = 3) then CalcPoint_triangle(ch,PointCount,Min,Max);
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
end;

procedure TForm1.cb_shapes_ch0Click(Sender: TObject);
begin
  Ch_Graph_Update(0,DAC_Data.count0,DAC_Data.min0,DAC_Data.max0);
end;

procedure TForm1.cb_shapes_ch1Click(Sender: TObject);
begin
  Ch_Graph_Update(1,DAC_Data.count1,DAC_Data.min1,DAC_Data.max1);
end;

procedure TForm1.cb_spi_speedChange(Sender: TObject);
begin
//  Signal_calc(Form1.cb_spi_speed.Text);
  Signal_calc(StringReplace(Form1.cb_spi_speed.Text, '.', ',',[rfReplaceAll]));
end;

procedure TForm1.Bit_dac_msbClick(Sender: TObject);
begin
  if (Bit_dac_msb.Caption = 'Reset MSB') then WriteStringComm(DAC_MSB+'00'+#13)
    else WriteStringComm(DAC_MSB+'01'+#13);
  Wait_Rx();
  WriteStringComm(DAC_RESET+#13);Wait_Rx();
end;

procedure TForm1.Bit_dac_rstClick(Sender: TObject);
begin
//TODO: Reset function
  if (Bit_dac_rst.Caption = 'Reset MSB') then WriteStringComm(DAC_MSB+'00'+#13)
    else WriteStringComm(DAC_MSB+'01'+#13);
  Wait_Rx();
  WriteStringComm(DAC_RESET+#13);Wait_Rx();
end;

procedure TForm1.Btn_dacClick(Sender: TObject);
begin
  WriteStringComm(DAC_RESET+#13);Wait_Rx();
end;

procedure Dac_cfg();
begin
  WriteStringComm(SPI_SET_SPEED+inttostr(trunc(strtofloat(StringReplace(Form1.cb_spi_speed.Text, '.', ',',[rfReplaceAll]))*1000000))+#13);Wait_Rx();
  if (Form1.rg_dac_load.ItemIndex = 1) then
  begin
    WriteStringComm(DAC_MODE_PARALLEL+'01'+#13);Wait_Rx();
  end else
  begin
    WriteStringComm(DAC_MODE_PARALLEL+'00'+#13);Wait_Rx();
    if (Form1.cb_dac_sync.Checked) then WriteStringComm(DAC_UPDATE_SYNC+'01'+#13)
      else WriteStringComm(DAC_UPDATE_SYNC+'00'+#13);
    Wait_Rx();
   end;
end;

procedure TForm1.Btn_dac_startClick(Sender: TObject);
begin
  Dac_cfg();

  WriteStringComm(DAC_BUFF_START+#13);Wait_Rx();
  Btn_dac_start.Enabled:=False;
  Btn_dac_stop.Enabled:=True;
  Btn_load_buff.Enabled:=False;
end;

procedure TForm1.Btn_dac_stopClick(Sender: TObject);
begin
  WriteStringComm(DAC_BUFF_STOP+#13);Wait_Rx();
  Btn_dac_start.Enabled:=True;
  Btn_dac_stop.Enabled:=False;
  Form1.Btn_load_buff.Enabled:=True;
end;

procedure TForm1.Btn_load0Click(Sender: TObject);
var P1,P2 : Pointer;
     S : String;
begin
  s:=' ';
  P1:= Pointer(S);
  P1:= Addr(DAC_Data.data[1]);
  P2:= Addr(DAC_Data.data[2]);
  Memo1.Lines.Add('Addr Data0 = '+IntToStr( Integer(P1))+' Addr Data1 = '+IntToStr( Integer(P2)));
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  KillComm;
  Application.Terminate;
end;

procedure TForm1.FormCreate(Sender: TObject);
const SPI_MAX_SPEED = 50;
var i:byte;
    f:real;
    s:string;
begin
// OpenDialog1.Filter := 'Wave files|*.wav';
//  f:=DAC_CLK_MAX;
  BufferRunning:=False;
  cb_spi_speed.Clear;
  for i := 1 to HIGH(SPI_SPEED)-1 do cb_spi_speed.Items.Add(SPI_SPEED[i]);
  cb_spi_speed.ItemIndex:=1;
  Memo1.Clear;
  GetComList(cb_comport);
  LoadConfig();
  AssignFile(File_log,FILE_LOG_NAME);
  Rewrite(File_log);
  WriteLn(File_log,TimeToStr(Now)+': Start Log File: '+FILE_LOG_NAME);
  CloseFile(File_log);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Sleep(400);
  KillComm;
  Application.Terminate;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  if (Form1.Height < 500) then Form1.Height:=500;
  if (Form1.Width < 692) then Form1.Width:=692;
//  if (Form1.Width <> 700) then Form1.Width:=700;


  Chart1.Width:=Form1.Width-10;
  Memo1.Width:=Form1.Width-15;
  Memo1.Height:= 60;//Form1.Height-Memo1.Top-StatusBar1.Height-20;
  Chart1.Height:=Form1.Height-Chart1.Top-Memo1.Height-StatusBar1.Height-60;
  Memo1.Top:=Chart1.Top+Chart1.Height;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  if (PortTimeOut>0) then L_timeout.Caption:='TimeOut = '+inttostr(PortTimeOut)+' ms';
  if ((DAC_DeviceCurrent < 1) AND {(NOT BufferRunning) AND }(McuConnected)) then DACSelectDev();
end;

function FloatValid(a:string):boolean;
const
  FloatSet: set of CHAR = ['0'..'9', '.',','];
var i:integer;
begin
  Result:=True;
  for i:=1 to length(a) do
    if(NOT (a[i] in FloatSet)) then Result:=False;
end;

procedure TForm1.Label7Click(Sender: TObject);
var fgoal:real;
    s:string;
begin
//  s:=StringReplace(InputBox('������� �������','������� �������, ���',''),'.',',');
  s:=InputBox('��������� �������','������� �������, ���','');
  if(FloatValid(s)) then fgoal:=strtofloat(s);
end;

procedure TForm1.L_frieq0Click(Sender: TObject);
var freq:real;
    cnt:integer;
    s:string;
begin
  s:=InputBox('��������� ������� (����� �)','�������, ��',InttoStr(Get_frieqHz(0)));
  if(FloatValid(s)) then
  begin
    freq:=strtofloat(s);
    cnt:=Frieq2Count(freq);
    if(cnt>0) then Ch_Graph_Update(0,cnt,DAC_Data.min0,DAC_Data.max0);
  end;
end;

procedure TForm1.L_frieq1Click(Sender: TObject);
var freq:real;
    cnt:integer;
    s:string;
begin
  s:=InputBox('��������� ������� (����� B)','�������, ��',InttoStr(Get_frieqHz(1)));
  if(FloatValid(s)) then
  begin
    freq:=strtofloat(s);
    cnt:=Frieq2Count(freq);
    if(cnt>0) then Ch_Graph_Update(1,cnt,DAC_Data.min1,DAC_Data.max1);
  end;
end;

procedure TForm1.L_timeoutClick(Sender: TObject);
var a:integer;
begin
  a:=strtoint(InputBox('����� �������� ������','TimeOut, ms','1000'));
  if (a>10) then
  begin
    PortTimeOut:= a;
    SaveConfig();
  end;
  L_timeout.Caption:='TimeOut = '+inttostr(a)+' ms';
end;

procedure TForm1.N01Click(Sender: TObject);
begin
  Gen_meandr(0);
end;

procedure TForm1.N02Click(Sender: TObject);
begin
  Gen_rise(0);
end;

procedure TForm1.N03Click(Sender: TObject);
begin
  Gen_triangle(0);
end;

procedure TForm1.N10Click(Sender: TObject);
const DemoData: array[1..50] of integer = (0,81,162,243,324,405,486,567,648,729,810,891,972,1053,1134,1215,1296,1377,1458,1539,1620,1701,1782,1863,1944,2025,2106,2187,2268,2349,2430,2511,2592,2673,2754,2835,2916,2997,3078,3159,3240,3321,3402,3483,3564,3645,3726,3807,3888,3969);
var i,pointcnt:integer;
begin
  pointcnt:=HIGH(DemoData);
  DAC_Data.count0:=pointcnt;
  DAC_Data.count1:=pointcnt;
  for i := 1 to pointcnt do
  begin
    DAC_Data.data[i].ch0:=DemoData[i];
    DAC_Data.data[i].ch1:=DAC_Data.data[i].ch0;
  end;
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
end;

procedure TForm1.N11Click(Sender: TObject);
begin
  Gen_meandr(1);
end;

procedure TForm1.N12Click(Sender: TObject);
begin
  Gen_rise(1);
end;

procedure TForm1.N13Click(Sender: TObject);
begin
  Gen_triangle(1);
end;

procedure TForm1.N14Click(Sender: TObject);
var str,s:string;
    i,pointcnt:integer;
begin
  pointcnt:=0;
  str:=InputBox('���� ����� �������','�������� �����','0,2,4,6');
//  Form1.L_shapes_ch0.Caption:='STRING';
//  Form1.L_shapes_ch1.Caption:='STRING';
  while (pos(',',str)>0) do
  begin
    inc(pointcnt);
    DAC_Data.data[pointcnt].ch0:=strtoint(copy(str,1,pos(',',str)-1));
    DAC_Data.data[pointcnt].ch1:=DAC_Data.data[pointcnt].ch0;
    delete(str,1,pos(',',str));
  end;
  if (length(str)>0) then
  begin
    inc(pointcnt);
    DAC_Data.data[pointcnt].ch0:=strtoint(str);
    DAC_Data.data[pointcnt].ch1:=DAC_Data.data[pointcnt].ch0;
  end;
  DAC_Data.count0:=pointcnt;
  DAC_Data.count1:=pointcnt;
  Graph_update();
  Signal_calc(Form1.cb_spi_speed.Text);
end;

procedure TForm1.N15Click(Sender: TObject);
begin
  fAbout.Show;
end;

procedure TForm1.N16Click(Sender: TObject);
begin
  Gen_String(0);
end;

procedure TForm1.N17Click(Sender: TObject);
begin
  Gen_String(1);
end;

procedure TForm1.N18Click(Sender: TObject);
begin
  Gen_String(2);
end;

procedure TForm1.NDemoClick(Sender: TObject);
begin
  if (NDemo.Checked) then NDemo.Checked:= False else NDemo.Checked:= True;
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  OpenDialog1.Filter := 'DAC1273 (NIIET) files|*.dac1273';
  if OpenDialog1.Execute() then
  begin
    Dac_read_file(OpenDialog1.FileName);
    Graph_update();
    Signal_calc(Form1.cb_spi_speed.Text);
  end;
end;

procedure TForm1.N3Click(Sender: TObject);
begin
 SaveDialog1.Filter := 'DAC1273 (NIIET) files|*.dac1273';
  if SaveDialog1.Execute() then
  begin
    Dac_save_file(SaveDialog1.FileName);
  end;

end;

procedure TForm1.N4Click(Sender: TObject);
begin
  Close();
end;

procedure TForm1.N6Click(Sender: TObject);
begin
  DAC_Read_All_Regs();
  DacRegsUpdate();
end;

procedure TForm1.N7Click(Sender: TObject);
begin
  Gen_meandr(2);
end;

procedure TForm1.N8Click(Sender: TObject);
begin
  Gen_rise(2);
end;

procedure TForm1.N9Click(Sender: TObject);
begin
  Gen_triangle(2);
end;

procedure TForm1.NconnectClick(Sender: TObject);
begin
  if (Nconnect.Caption='����������') then
  begin
    if (NOT NDemo.Checked) then
    begin
      KillComm;
      PortInit(cb_Comport.Text);
      WriteStringComm(DAC_BUFF_STOP+#13);
      Sleep(100);
      PurgeComm(CommHandle, PURGE_RXCLEAR);
      RxBuff:='';
      RxFlag:=False;
      McuConnected:=false;
      RxBuff_timeout:=false;
    end else McuConnected:=true;
//    WriteStringComm(DAC_BUFF_STOP+#13);Sleep(50); PurgeComm(CommHandle, PURGE_RXCLEAR);
    WriteStringComm(DAC_INFO+#13); Wait_Rx();
    if (McuConnected) then
    begin
      StatusBar1.Update;
      Form1.Show;
      Btn_load_buff.Enabled:=True;
      Btn_dac_start.Enabled:=False;
      Btn_dac_stop.Enabled:=False;
      Bit_dac_msb.Enabled:=True;
      Add_log('DAC1273 Is Connected, '+HW_Version);
      StatusBar1.Panels[1].Text:=cb_Comport.Text+' Open';
      Form1.StatusBar1.Panels[2].Text:='';
      WriteStringComm(DAC_BUFF_MAXPOINT+#13);  Wait_Rx();
      WriteStringComm(DAC_MSB+'01'+#13);  Wait_Rx();
      SaveConfig();
      DACSelectDev();
    end else
    begin
      Btn_load_buff.Enabled:=False;
      Btn_dac_start.Enabled:=False;
      Btn_dac_stop.Enabled:=False;
      Bit_dac_msb.Enabled:=False;
      KillComm;
      Add_log('DAC1273 Not Connected');
    end;
    Nconnect.Caption:='���������';
  end else
  begin
    Nconnect.Caption:='����������';
    KillComm;
    McuConnected:=false;
    StatusBar1.Update;
    Form1.Show;
    Btn_load_buff.Enabled:=False;
      Btn_dac_start.Enabled:=False;
      Btn_dac_stop.Enabled:=False;
      Bit_dac_msb.Enabled:=False;
    Add_log('DAC1273 is Closed');
    StatusBar1.Panels[1].Text:=cb_Comport.Text+' Close';
  end;
end;

procedure TForm1.Nsin_2channelClick(Sender: TObject);
begin
  Gen_sin(2);
end;

procedure TForm1.Nsin_ch0Click(Sender: TObject);
begin
  Gen_sin(0);
end;

procedure TForm1.Nsin_ch1Click(Sender: TObject);
begin
  Gen_sin(1);
end;

procedure TForm1.N_fullClick(Sender: TObject);
begin
  if (N_full.Checked) then N_full.Checked:=False else N_full.Checked:=True;

end;

procedure TForm1.rg_dac_loadClick(Sender: TObject);
begin
  Signal_calc('');
end;

procedure TForm1.StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
  const Rect: TRect);
begin
//  if (Panel = StatusBar1.Panels[0]) then
//  begin
{    Canvas.Brush.Color:=clRed;
    Canvas.Pen.Color:=clBlack;
    Canvas.Ellipse(5,5,20,20);
    CAnvas.FillRect(Rect);
  }
    Canvas.Brush.Color:=$00F8FAFA;
    Canvas.FillRect(Rect);
    Canvas.Brush.Color:=$00CCDBDB;
    Canvas.FrameRect(Rect);
//  end;
  if (McuConnected) then StatusBar.Canvas.Brush.Color:=clGreen
    else StatusBar.Canvas.Brush.Color:=clRed;
  StatusBar.Canvas.Ellipse(Rect.Left,Rect.Top,Rect.Left+Rect.Height,Rect.Top+Rect.Height);
//  StatusBar.Panels[1].Text:='L='+inttostr(Rect.Left)+' W='+inttostr(Rect.Width)+' T='+inttostr(Rect.Top)+' H='+inttostr(Rect.Height);

end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  if (buff_count > 0) then
  begin
    Wait_Rx();
    dec(buff_count);
  end else Timer1.Enabled:=false;
  if (RxBuff_timeout) then Timer1.Enabled:=false;
end;

procedure TForm1.wav1Click(Sender: TObject);
var
  F: TFileStream;
  SampleCount, SamplesPerSec: integer;
  BitsPerSample, Channeles: smallint;
begin
  OpenDialog1.Filter := 'Wave files|*.wav';
  if (OpenDialog1.Execute) then ReadWave(OpenDialog1.FileName);
{
  // ����� OpenDialog1:
  if not OpenDialog1.Execute then
    Exit;
  try
    // �������� �����:
    F := TFileStream.Create(OpenDialog1.FileName, fmOpenRead);
    Memo1.Clear;
    Memo1.Lines.Add('Open Stream: ');
    // ������ ���������:
    ReadWaveHeader(F, SampleCount, SamplesPerSec,
      BitsPerSample, Channeles);
    Memo1.Lines.Add('Read header: ');
    F.Free;

    // ���������� Memo ����������� � �����:
    Memo1.Lines.Add('SampleCount: ' + IntToStr(SampleCount));
    Memo1.Lines.Add(Format('Length: %5.3f sec', [SampleCount / SamplesPerSec]));
    Memo1.Lines.Add('Channeles: ' + IntToStr(Channeles));
    Memo1.Lines.Add('Freq: ' + IntToStr(SamplesPerSec));
    Memo1.Lines.Add('Bits: ' + IntToStr(BitsPerSample));
  except
    raise Exception.Create('Problems with file reading');
  end;
  }
end;

end.
