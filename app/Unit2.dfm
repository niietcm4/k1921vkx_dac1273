object fAbout: TfAbout
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  ClientHeight = 509
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 312
    Top = 71
    Width = 138
    Height = 16
    Caption = #1057#1093#1077#1084#1072' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 336
    Top = 471
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 766
    Height = 65
    BorderStyle = bsNone
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      
        #1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1076#1077#1084#1086#1085#1089#1090#1088#1080#1088#1091#1077#1090' '#1091#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1087#1088#1077#1094#1080#1079#1080#1086#1085#1085#1099#1084#1080' '#1062#1040#1055'  '#1089' '#1087#1086#1084#1086#1097#1100#1102' ' +
        #1084#1080#1082#1088#1086#1082#1086#1085#1090#1088#1086#1083#1083#1077#1088#1086#1074' K1921VK01T (K1921VK035) '
      #1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072' '#1040#1054' "'#1053#1048#1048#1069#1058'".'
      
        #1042' '#1087#1088#1086#1075#1088#1072#1084#1084#1077' '#1088#1077#1072#1083#1080#1079#1086#1074#1072#1085#1099' '#1072#1083#1075#1086#1088#1080#1090#1084#1099' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1090#1077#1089#1090#1086#1074#1099#1093' '#1089#1080#1075#1085#1072#1083#1086#1074 +
        ': '#1057#1080#1085#1091#1089', '#1052#1077#1072#1085#1076#1088', '#1053#1072#1088#1072#1089#1090#1072#1102#1097#1080#1081' '#1092#1088#1086#1085#1090', '#1058#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082' '#1080' '
      #1074#1074#1086#1076' '#1089#1090#1088#1086#1082#1080' '#1079#1085#1072#1095#1077#1085#1080#1081' ('#1088#1072#1079#1076#1077#1083#1080#1090#1077#1083#1100': "'#1079#1072#1087#1103#1090#1072#1103'").')
    ParentFont = False
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 93
    Width = 106
    Height = 372
    Caption = #1042#1099#1073#1086#1088' '#1062#1040#1055#1072
    TabOrder = 2
    object sb_dac034: TSpeedButton
      Left = 3
      Top = 24
      Width = 100
      Height = 22
      GroupIndex = 1
      Down = True
      Caption = '1273HA03'#1040'4'
      OnClick = sb_dac034Click
    end
    object sb_dac015: TSpeedButton
      Left = 3
      Top = 52
      Width = 100
      Height = 22
      GroupIndex = 1
      Caption = '1273HA015/025'
      OnClick = sb_dac034Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 112
    Top = 93
    Width = 644
    Height = 372
    Caption = #1042#1099#1073#1086#1088' '#1091#1087#1088#1072#1074#1083#1103#1102#1097#1077#1075#1086' '#1084#1080#1082#1088#1086#1082#1086#1085#1090#1088#1086#1083#1083#1077#1088#1072
    TabOrder = 3
    object TabControl1: TTabControl
      Left = 5
      Top = 25
      Width = 633
      Height = 340
      Style = tsFlatButtons
      TabOrder = 0
      Tabs.Strings = (
        'K1921VK01T'
        'K1921VK035')
      TabIndex = 0
      OnChange = TabControl1Change
      object Image_sch: TImage
        Left = 4
        Top = 27
        Width = 625
        Height = 309
        Align = alClient
        Proportional = True
        Stretch = True
        ExplicitLeft = 244
        ExplicitTop = 91
      end
    end
  end
end
