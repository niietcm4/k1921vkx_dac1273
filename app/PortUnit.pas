unit PortUnit;


interface
 uses windows,sysutils,Registry,StdCtrls, Classes;
const
   DAC_1273HA015 = '1273HA015';
   DAC_1273HA025 = '1273HA025';
   DAC_1273HA034 = '1273HA03�4';
   DAC_1273HA015_NUM = 1;
   DAC_1273HA025_NUM = 2;
   DAC_1273HA034_NUM = 3;
 var
    CommHandle : integer;
    DCB : TDCB;
    Ovr : TOverlapped;
    Stat : TComStat;
    CommThread : THandle;
    hEvent : THandle;
    Flag,StopResive : boolean;
    KolByte,Kols,Mask,TransMask,Errs : DWord;
    last_comm:string[50];
    RxBuff:string[255];
    Rxcount:byte;
    RxFlag:Boolean;
    McuConnected:boolean;
    RxBuff_timeout:boolean;
    RxEn:boolean;
    Dac_CRC32:Cardinal;
    Dac_BuffCount:Word;
    Dac_BuffUpdate:boolean;
    Dac_Regs:array[0..13] of Word;
    HW_Version:string;
    RxStrList:TStringList;
    BufferRunning:Boolean;
    PortTimeOut:Integer;
    BufferOverFlow:Boolean;
 procedure PortInit(ComPortName:string);
 procedure ReadComm;
 procedure WriteComm(A:byte);
 procedure KillComm;
 procedure GetComList(var CBox1: TComboBox);
 procedure WriteBuffComm(var Buf; Size: DWord);
 procedure WriteStringComm(str:string);
 procedure Wait_Rx();
 procedure DacRegsUpdate();
 function GetRxListCount():integer;

 implementation

 uses
  Unit1;

procedure GetComList(var CBox1: TComboBox);
var
  i,k: integer;
  reg : TRegistry;
  ts : TStrings;
begin
  CBox1.Clear;
  k:=-1;
  reg := TRegistry.Create(KEY_READ or KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
//  reg.OpenKey('SOFTWARE\Microsoft\Windows NT\CurrentVersion\Ports',false);
  reg.OpenKey('HARDWARE\DEVICEMAP\SERIALCOMM',false);
  ts := TStringList.Create;
  //reg.GetKeyNames(ts);
  reg.GetValueNames(ts);
  for i := 0 to ts.Count -1 do
  begin
    //if (pos('COM',ts.Strings[i])>0) then
    CBox1.Items.Add(reg.ReadString(ts.Strings[i]));
  end;
  ts.Free;
  reg.CloseKey;
  reg.free;
  if (CBox1.Items.Count = 0) then CBox1.Items.Add('');
  if (CBox1.Items.Count > 1) then CBox1.ItemIndex:=CBox1.Items.Count-1
  else if (CBox1.Items.Count > 0) then CBox1.ItemIndex:=0;
end;

 procedure KillComm;
 begin
  RxEn:=false;
  PurgeComm(CommHandle, PURGE_TXCLEAR);
  PurgeComm(CommHandle, PURGE_RXCLEAR);
  TerminateThread(CommThread,0);
//  if (RxStrList <> Nil) then RxStrList.Free;
  CloseHandle(CommHandle);
//  BufferRunning:=False;
 end;

 function GetRxListCount():integer;
 begin
   Result:=RxStrList.Count;
 end;

 procedure WriteComm(A:byte);
 var
  Transmit:array [0..255] of char;
 begin
  //������� ���� ������ � ����
   KolByte:=1;
   Transmit[0]:=chr(A);
   WriteFile(CommHandle,Transmit,KolByte,KolByte,@Ovr);
 end;

 procedure WriteBuffComm(var Buf; Size: DWord);
 var
  Transmit:array [0..255] of char;

 begin
  //������� ���� ������ � ����
   WriteFile(CommHandle,Buf,Size,Size,@Ovr);
 end;

 procedure WriteStringComm(str:string);
 var
  Transmit:array [0..255] of char;
//  trans_byte:array [0..255] of byte;
  WriteBytes: array[0..255] of Byte;
  i:integer;
  s:string;
  COMStat: TCOMStat; // ���������� ��� ����������� ������ � ������
  xnH: DWORD;
  xn: Int64;
 begin
   if (NOT Form1.NDemo.Checked) then
   begin
     KolByte:=length(str);
     PurgeComm(CommHandle, PURGE_TXCLEAR);
     PurgeComm(CommHandle, PURGE_RXCLEAR);
     s:='';
     for i := 0 to length(str)-1 do
     begin
       WriteBytes[i]:=ORD(str[i+1]);
       s:=s+CHR(WriteBytes[i]);
     end;

{   for i := 1 to KolByte do
   begin
     Transmit[i-1]:=str[i];
     trans_byte[i-1]:=ORD(str[i]);
     s:=s+CHR(trans_byte[i-1]);
   end;}

   if ((NOT BufferRunning) OR (pos(DAC_BUFF_STOP,s)>0) OR (pos(DAC_INFO,s)>0)) then
   begin
     Add_log('DAC1273> '+s);
     WriteFile(CommHandle,WriteBytes,KolByte,KolByte,@Ovr);
   end;
   last_comm:=s;
   if (pos(DAC_BUFF_START,last_comm)>0) then BufferRunning:=True;
   if (pos(DAC_BUFF_STOP,last_comm)>0) then BufferRunning:=False;
  //������� ���� ������ � ����
//   WriteFile(CommHandle,Transmit,KolByte,KolByte,@Ovr);


//   ClearCommError(CommHandle,xnH,@COMStat); //��������    ���������    COM �   StatCOM
//   xn:=COMStat.cbInQue;  // �  cblnQue ������ ����� ���� ����������  ������ �  ������
//   Form1.StatusBar1.Panels[1].Text:='Count= '+inttostr(xn);
    end else
    begin
      Add_log('DAC1273> '+str);
      last_comm:=str;
    end;
end;


 procedure ReadComm;
  var
   Resive:array [0..2048] of Ansichar;
   str,s:string;
   i:integer;
  begin
   while (RxEn) do
   begin
    TransMask:=0;
    WaitCommEvent(CommHandle,TransMask,@Ovr); //����
    if (TransMask and EV_RXFLAG)=EV_RXFLAG then //��������� ������ �������
//    if (TransMask and EV_RXCHAR)=EV_RXCHAR then //��������� ������ �������
     begin
      ClearCommError(CommHandle,Errs,@Stat);//���������� ����
      Kols := Stat.cbInQue;
      if (Kols>0) then
      begin
        ReadFile(CommHandle,Resive,Kols,Kols,@Ovr);//������
      //��� ������ ���� ��������� �������� ���������� �� Resive
        if (Kols>0) then
        begin
          if (Kols > HIGH(Resive)) then s:=copy(string(Resive),1,HIGH(Resive))
          else s:=copy(string(Resive),1,Kols);
          str:=str+s;
          if (pos(#13,str)>0) then
          begin
            while (ord(str[length(str)])<14) do delete(str,length(str),1);
            RxBuff:=str;
            RxStrList.Add(str);
            Rxcount:=length(str);
            str:='';
            RxFlag:=true;
          end;
        end;
      end;
     end;//mask
    end;//while
    PurgeComm(CommHandle, PURGE_RXCLEAR);
  end;

 procedure PortInit(ComPortName:string);
  var
   ThreadID:dword;
  begin
  //�������� � ������������ �����
  KolByte:=0;

  //�������� ����� � ��������� ��� ������
  CommHandle := CreateFile(PChar(ComPortName),GENERIC_READ or GENERIC_WRITE,0,nil,OPEN_EXISTING,
         FILE_ATTRIBUTE_NORMAL or FILE_FLAG_OVERLAPPED,0);

  //������ ����� - "�� ���������� ������������� �������"
  SetCommMask(CommHandle,EV_RXFLAG);
//  SetCommMask(CommHandle,EV_RXCHAR);

  //���������� DCB
   GetCommState(CommHandle,DCB);
   DCB.BaudRate:=CBR_115200;
   DCB.Parity:=NOPARITY;
   DCB.ByteSize:=8;
   DCB.StopBits:=OneStopBit;

   DCB.EvtChar:=chr(13);//������� ������� ��� �����
   //������������� DCB
   SetCommState(CommHandle,DCB);

   SetUpComm(CommHandle,20000,20000);
   PurgeComm(CommHandle, PURGE_TXCLEAR);
   PurgeComm(CommHandle, PURGE_RXCLEAR);


   //������� TStringList ��� ���������� �������� �����
   RxStrList:=TStringList.Create;
   //������� ����������� �����
   //��� ����� ��������� ��������� ������ ������
   //� ����� - ReadComm
   RxEn:=true;
   CommThread := CreateThread(nil,0,@ReadComm,nil,0,ThreadID);
  // Set DTR
   EscapeCommFunction( CommHandle, SETDTR);
  // Clear DTR
   EscapeCommFunction( CommHandle, CLRDTR);
  end;

function DacBuffCount(a:string):Word;
var i:byte;
begin
  Result:=0;
  i:= pos(DAC_BUFF_COUNT,a)+length(DAC_BUFF_COUNT)+1;
  if (pos(DAC_BUFF_COUNT,a)>0) then Result:=strtoint(copy(a,i,length(a)-i));
end;

function DacBuffCRC(a:string):Cardinal;
var i:byte;
    s:string;
begin
  Result:=0;
  i:= pos(DAC_BUFF_CRC32_ANSWER,a)+length(DAC_BUFF_CRC32_ANSWER)+1;
  s:= trim(copy(a,i,length(a)-i+1));
  if (pos(DAC_BUFF_CRC32_ANSWER,a)>0) then Result:=HEX2DEC(s);
end;

procedure DacRegsUpdate();
var i:integer;
begin
{  for i := 0 to High(Dac_Regs) do
    if (Dac_Regs[i]>15) then Form1.sg_dac_regs.Cells[1,i]:=DEC2HEX(Dac_Regs[i])
      else Form1.sg_dac_regs.Cells[1,i]:='0'+DEC2HEX(Dac_Regs[i]);
 }
end;

function DacRegRead(a:string):Cardinal;
var i:byte;
    addr:byte;
    s,data_str:string;
begin
  Result:=0;
 //     DAC_REG_READ_ANSWER = 'RREG ';
 //     DAC_REG_READ_ANSWER_DATA = ' = ';
  i:= pos(DAC_REG_READ_ANSWER,a)+length(DAC_REG_READ_ANSWER)+1;
  s:= trim(copy(a,i,length(a)-i+1));
  if (pos(DAC_REG_READ_ANSWER_DATA,s)>0) then
  begin
    addr:=HEX2DEC(copy(s,1,pos(DAC_REG_READ_ANSWER_DATA,s)-1));
    i:= pos(DAC_REG_READ_ANSWER_DATA,s)+length(DAC_REG_READ_ANSWER_DATA);
    data_str:= trim(copy(s,i,length(s)-i+1));
    while(length(data_str)<8) do data_str:='0'+data_str;
    if (addr<HIGH(Dac_Regs)) then
    begin
      Dac_Regs[addr]:=HEX2DEC(copy(data_str,7,2));
      if (addr+1<HIGH(Dac_Regs)) then Dac_Regs[addr+1]:=HEX2DEC(copy(data_str,5,2));
      if (addr+2<HIGH(Dac_Regs)) then Dac_Regs[addr+2]:=HEX2DEC(copy(data_str,3,2));
      if (addr+3<HIGH(Dac_Regs)) then Dac_Regs[addr+3]:=HEX2DEC(copy(data_str,1,2));
    end;
    Result:=HEX2DEC(data_str);
    DacRegsUpdate();
  end;
end;

function DacBuffRead(a:string):Cardinal;
var i:byte;
    addr:byte;
    s,data_str:string;
begin
  Result:=0;
  i:= pos(DAC_BUFF_READ_ANSWER,a)+length(DAC_BUFF_READ_ANSWER)+1;
  s:= trim(copy(a,i,length(a)-i+1));
  if (pos(DAC_BUFF_READ_ANSWER,a)>0) then
  begin
    addr:=HEX2DEC(copy(s,1,pos(DAC_BUFF_READ_ANSWER,s)-1));
    i:= pos(DAC_REG_READ_ANSWER_DATA,s)+length(DAC_REG_READ_ANSWER_DATA);
    data_str:= trim(copy(s,i,length(s)-i+1));
    while(length(data_str)<8) do data_str:='0'+data_str;
    if (addr<HIGH(Dac_Regs)) then
    begin
      DAC_Data_out.data[addr].ch0:=HEX2DEC(copy(data_str,5,4));
      DAC_Data_out.data[addr].ch1:=HEX2DEC(copy(data_str,1,4));
    end;
  end;
end;


function DacRegReadB(a:string):Word;
var i:byte;
    addr:byte;
    s,data_str:string;
begin
  Result:=0;
 //     DAC_REG_READ_ANSWER = 'RREG ';
 //     DAC_REG_READ_ANSWER_DATA = ' = ';
   i:= pos(DAC_REG_READB_ANSWER,a)+length(DAC_REG_READ_ANSWER)+1;
  s:= trim(copy(a,i,length(a)-i+1));
  if (pos(DAC_REG_READ_ANSWER_DATA,s)>0) then
  begin
    addr:=HEX2DEC(copy(s,1,pos(DAC_REG_READ_ANSWER_DATA,s)-1));
    i:= pos(DAC_REG_READ_ANSWER_DATA,s);
    i:= pos(DAC_REG_READ_ANSWER_DATA,s)+length(DAC_REG_READ_ANSWER_DATA);
    data_str:= trim(copy(s,i,length(s)-i+1));
    while(length(data_str)<2) do data_str:='0'+data_str;
    Dac_Regs[addr]:=HEX2DEC(data_str);
{    if (addr<HIGH(Dac_Regs)) then
    begin
      Dac_Regs[addr]:=HEX2DEC(copy(data_str,7,2));
      if (addr+1<HIGH(Dac_Regs)) then Dac_Regs[addr+1]:=HEX2DEC(copy(data_str,5,2));
      if (addr+2<HIGH(Dac_Regs)) then Dac_Regs[addr+2]:=HEX2DEC(copy(data_str,3,2));
      if (addr+3<HIGH(Dac_Regs)) then Dac_Regs[addr+3]:=HEX2DEC(copy(data_str,1,2));
    end; }
    Result:=HEX2DEC(data_str);
  end;
end;

procedure GetMSB(a:string);
begin
  if (pos(DAC_MSB_ANSWER_RESET,a)>0) then begin Form1.Bit_dac_msb.Caption:='Set MSB'; Form1.StatusBar1.Panels[4].Text:='MSB = LOW'; end
    else if (pos(DAC_MSB_ANSWER_SET,a)>0) then begin Form1.Bit_dac_msb.Caption:='Reset MSB'; Form1.StatusBar1.Panels[4].Text:='MSB = HIGH'; end;
end;

procedure GetDevDAC(a:string);
begin
  if (pos(DAC_1273HA015,a)>0) then  begin Form1.StatusBar1.Panels[3].Text:='��� '+DAC_1273HA015; DAC_DeviceCurrent:=DAC_1273HA015_NUM; end
    else if (pos(DAC_1273HA025,a)>0) then begin Form1.StatusBar1.Panels[3].Text:='��� '+DAC_1273HA025; DAC_DeviceCurrent:=DAC_1273HA025_NUM; end
      else if (pos(DAC_1273HA034,a)>0) then begin Form1.StatusBar1.Panels[3].Text:='��� '+DAC_1273HA034; DAC_DeviceCurrent:=DAC_1273HA034_NUM; end;
end;

procedure GetMaxPoint(a:string);
var tmp:integer;
begin
  tmp:= pos(DAC_BUFF_MAXPOINT_ANSWER,a)+length(DAC_BUFF_MAXPOINT_ANSWER)-1;
  if ( pos(DAC_BUFF_MAXPOINT_ANSWER,a)>0) then DAC_MaxPoint:=strtoint(copy(a,tmp+1,length(a)-tmp));
  if (DAC_MaxPoint > 0) then Add_log(' ������������ ���������� ����� '+inttostr(DAC_MaxPoint));

end;

procedure Wait_Rx();
const TimeOut = 1000;
var cnt,i:integer;
    currTimeOut:integer;
    s,DemoRxBuff,tmp:string;
    iCounterPerSec: TLargeInteger;
    C1, C2: TLargeInteger;
begin
//  QueryPerformanceFrequency(iCounterPerSec);
//  QueryPerformanceCounter(C1);
  if (NOT Form1.NDemo.Checked) then
  begin


  if (PortTimeOut>10) then cnt:=PortTimeOut
   else cnt:=TimeOut;
   currTimeOut:=cnt;
  while((cnt > 0) AND (NOT RxFlag)) do
  begin
    dec(cnt);
    sleep(1);
  end;
  if (RxFlag) then
  begin
   Add_log('    MCU('+inttostr(currTimeOut-cnt)+'mSec)> '+RxBuff);
   Form1.StatusBar1.Panels[0].Text:=Form1.cb_Comport.Text+': receive '+inttostr(Rxcount)+' Byte';
   RxFlag:=false;
   Rxcount:=0;
   if (pos(DAC_OVERFLOW_ANSWER,RxBuff)>0) then BufferOverFlow:=True;
   if (pos(DAC_INFO,last_comm)>0) then if ((pos(DAC_INFO_ANSWER,RxBuff)>0) OR (pos(DAC_INFO_ANSWER2,RxBuff)>0)) then
   begin
     Mcuconnected:=true;
     if (pos(DAC_INFO_ANSWER,RxBuff)>0) then i:=pos(DAC_INFO_ANSWER,RxBuff)+length(DAC_INFO_ANSWER)
       else i:=pos(DAC_INFO_ANSWER2,RxBuff)+length(DAC_INFO_ANSWER2);
     HW_Version:=copy(RxBuff,i+1,length(RxBuff)-i);
   end;
   if (pos(DAC_BUFF_COUNT,last_comm)>0) then Dac_BuffCount:=DacBuffCount(RxBuff)
   else if (pos(DAC_BUFF_UPDATE,last_comm)>0) then
   begin
     {
     if (pos(DAC_BUFF_UPDATE_ANSWER,RxBuff)>0) then
     begin
       Dac_BuffUpdate:=True;
       if (pos('is Error',RxBuff)>0) then Form1.Btn_buff_err.Enabled:=True
         else Form1.Btn_buff_err.Enabled:=False;
     end else Dac_BuffUpdate:=False;
     }
   end
   else if (pos(DAC_DEVICE,last_comm)>0) then
   begin
     if (NOT Form1.NDemo.Checked) then
       GetDevDAC(RxBuff)
       else
       begin
         tmp:='';
         if(Form1.cb_device.ItemIndex = 0) then GetDevDAC('DAC Select Device: '+DAC_1273HA025+' ')
           else
       end;

   end
   else if (pos(DAC_BUFF_MAXPOINT,last_comm)>0) then GetMaxPoint(RxBuff)
   else if (pos(DAC_MSB,last_comm)>0) then GetMSB(RxBuff)
   else if (pos(DAC_BUFF_CRC32,last_comm)>0) then Dac_CRC32:=DacBuffCRC(RxBuff)
   else if (pos(DAC_REG_READ,last_comm)>0) then DacRegRead(RxBuff)
   else if (pos(DAC_REG_READB,last_comm)>0) then DacRegReadB(RxBuff);
   RxBuff:='';
  end else
  begin
//    QueryPerformanceCounter(C2);
    Add_log(' Error TimeOut ');//+FormatFloat('0.0', (C2 - C1)*1000 / iCounterPerSec) + ' ����.');
    if (McuConnected) then McuConnected:=false;
    RxBuff_timeout:=true;
    Form1.Nconnect.Caption:='����������';
    Form1.Btn_load_buff.Enabled:=False;
  end;
  end else
  begin

   if (pos(DAC_INFO,last_comm)>0) then
   begin
     DemoRxBuff:=DAC_INFO_ANSWER;
     Mcuconnected:=true;
   end
    else if (pos(DAC_DEVICE,last_comm)>0) then
    begin
      if (Form1.cb_device.ItemIndex = 0) then tmp:=DAC_1273HA015
        else if (Form1.cb_device.ItemIndex = 1) then tmp:=DAC_1273HA025
        else if (Form1.cb_device.ItemIndex = 2) then tmp:=DAC_1273HA034;
      GetDevDAC(tmp);
      DemoRxBuff:=tmp;
    end
    else if (pos(DAC_BUFF_MAXPOINT,last_comm)>0) then
    begin
      tmp:=DAC_BUFF_MAXPOINT_ANSWER+inttostr(2000);
      GetMaxPoint(tmp);
      DemoRxBuff:=tmp;
    end
    else if (pos(DAC_MSB,last_comm)>0) then GetMSB(RxBuff)
    else if (pos(DAC_BUFF_CRC32,last_comm)>0) then Dac_CRC32:=DacBuffCRC(RxBuff)
    else if (pos(DAC_REG_READ,last_comm)>0) then DacRegRead(RxBuff)
    else if (pos(DAC_REG_READB,last_comm)>0) then DacRegReadB(RxBuff);
    Add_log('    MCU*DEMO*(1mSec)> '+DemoRxBuff);
  end;
end;
end.

