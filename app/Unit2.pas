unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ImgList, Vcl.Buttons;

type
  TfAbout = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Label1: TLabel;
    TabControl1: TTabControl;
    Image_sch: TImage;
    GroupBox1: TGroupBox;
    sb_dac034: TSpeedButton;
    sb_dac015: TSpeedButton;
    GroupBox2: TGroupBox;
    procedure Button1Click(Sender: TObject);
    procedure SelectSch(a:byte);
    procedure UpdateSch();
    procedure FormShow(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure sb_dac034Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAbout: TfAbout;

implementation

{$R *.dfm}


procedure TfAbout.Button1Click(Sender: TObject);
begin
  Hide;
end;

procedure TfAbout.FormShow(Sender: TObject);
begin
  UpdateSch();
end;

procedure TfAbout.sb_dac034Click(Sender: TObject);
begin
  UpdateSch();
end;

procedure TfAbout.SelectSch(a:byte);
const IMG_RES_LIST:array[0..3] of string = ('PngImg_1','PngImg_2','PngImg_3','PngImg_4');
{PngImg_3 BITMAP "img\\png\\K1273HA015_K1921VK01T.png"
PngImg_4 BITMAP "img\\png\\K1273HA015_K1921VK035.png"
PngImg_1 BITMAP "img\\png\\K1273HA03A4_K1921VK01T.png"
PngImage_1 RCDATA "img\\png\\K1273HA03A4_K1921VK035.png"}
var //ResStream: TResourceStream;
//    SchBmp: TBitmap;
    png: TPNGObject;
begin
//  SchBmp:= TBitmap.Create;
  png := TPNGObject.Create;
//  Image_sch.Canvas.FillRect(Image_sch.ClientRect);
//  ImageList1.GetBitmap(a, Image_sch.Picture.Bitmap);
//  ResStream := TResourceStream.Create(HInstance, IMG_RES_LIST[a], RT_RCDATA); // Image1 - ��� �� ������� �������� � ����� ��������, ��� ���� � ��������
{  if (a = 0) then ResStream := TResourceStream.Create(HInstance, 'PngImg_1', RT_RCDATA)
  else  if (a = 1) then ResStream := TResourceStream.Create(HInstance, 'PngImg_2', RT_RCDATA)
  else  if (a = 2) then ResStream := TResourceStream.Create(HInstance, 'PngImg_3', RT_RCDATA)
  else  if (a = 3) then ResStream := TResourceStream.Create(HInstance, 'PngImg_4', RT_RCDATA);}
//  Image_sch.Picture.Bitmap.LoadFromStream(ResStream);   }
  png.LoadFromResourceName(HInstance, IMG_RES_LIST[a]);
  Image_sch.Picture.Assign(png);
  png.Free;
//  SchBmp.LoadFromResourceName(HInstance, IMG_RES_LIST[a]);
//  Image_sch.Picture.Assign(SchBmp);
//  SchBmp.Free;
//  ResStream.Free;
end;

procedure TfAbout.TabControl1Change(Sender: TObject);
begin
  UpdateSch();
end;

procedure TfAbout.UpdateSch();
var i:byte;
begin
  if (sb_dac034.Down) then
  begin
    if (TabControl1.TabIndex = 0) then i:=0 else if (TabControl1.TabIndex = 1) then i:=1;
  end else
  if (sb_dac015.Down) then
  begin
    if (TabControl1.TabIndex = 0) then i:=2 else if (TabControl1.TabIndex = 1) then i:=3;
  end;
  SelectSch(i);
end;

end.
