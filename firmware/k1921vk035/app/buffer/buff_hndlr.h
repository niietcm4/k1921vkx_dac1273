/*==============================================================================
 * Определения для обработки команд буфера данных для К1921ВК01Т
 *------------------------------------------------------------------------------
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

#include <stdint.h>
#include "buffer.h"
#include "DAC1273.h"
#include "bsp.h"

#define MAX_COMMANDS  20
#define DAC_BUFF_SIZE 2048

#define DAC_DIV_MAX		120

#define DAC_CH(X)   (*((uint32_t *) &X))

#define BUFF_COMM_SIZE(X) (sizeof(X)-1)

#define CMD_CLEAR "clear"
// Commands DAC1273 list

//Macro Validate Command
#define CASE_CMD(X)	(strncmp(GetPBuf(), X, sizeof(X)-1)==0)

// Commands Buffer
#define CMD_INFO 			"INFO"
#define CMD_HELP 			"HELP"
#define CMD_BUFF_MAXPOINT 	"BMPOINT"
#define CMD_BUFF_ADD 		"BADD "
#define CMD_BUFF_ADDN		"BADDN "
#define CMD_BUFF_GET  	  	"BGET "
#define CMD_BUFF_ALL_GET 	"BAGET "
#define CMD_BUFF_SIZE  		"BSIZE "
#define CMD_BUFF_COUNT  	"BCOUNT"
#define CMD_BUFF_CRC32  	"BCRC"
#define CMD_BUFF_START  	"BSTRT "
#define CMD_BUFF_STOP  	  	"BSTP "
#define CMD_DAC_UPDATE_SYN 	"DUPSN "
#define CMD_DAC_DEV_SEL   	"DDSEL "
#define CMD_DAC_MSB 		"DMSB "
#define CMD_DAC_RESET 		"DRST" 
#define CMD_DAC_MODE_LOAD_PARALLEL "DMLD "

//DAC Device
#define DAC_1273HA015    	1
#define DAC_1273HA025    	2
#define DAC_1273HA034    	3

#define DAC_1273HA034_NUM	34
#define DAC_1273HA015_NUM	15
#define DAC_1273HA025_NUM	25

#define CMD_GET_SYSCLK    	"SYSCLK"
// Commands SPI
#define CMD_GET_SPISPEED   	"SGSPEED"
#define CMD_SPI_SETSPEED  	"SSSPEED "

//Verion
#define MAJOR_VER_NUM		1
#define MINOR_VER_NUM		0
#define BUILD_NUM			1
#define REV_NUM				0

#define GET_INFO_VER printf("K1921VK035_DAC1273NA HW_Ver.%i.%i.%i.%i Compile: %s %s\r\n",MAJOR_VER_NUM,MINOR_VER_NUM,BUILD_NUM,REV_NUM,__DATE__,__TIME__)

typedef struct
{
	 uint8_t data[3];
}Dac_Dat_TypeDef;



void BuffHandler(void);

