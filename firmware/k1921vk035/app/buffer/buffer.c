/*==============================================================================
 * ������� ������ � ����� ������ �� ������ UART ��� �1921��01�
 *------------------------------------------------------------------------------
 * �����, ��������� ����� <dykhno@niiet.ru>
 *==============================================================================
 * ������ ����������� ����������� ��������������� ���� ���ܻ, ��� �����-����
 * ��������, ���� ���������� ��� ���������������, ������� �������� ��������
 * �����������, ������������ �� ��� ����������� ���������� � ����������
 * ���������, �� �� ������������� ���. ������ ����������� �����������
 * ������������� ��� ��������������� ����� � ���������� ������ ��
 * �������������� �������������� ���������� � ��������, � ����� ��������� �����
 * �����������. �� � ����� ������ ������ ��� ��������������� �� �����
 * ��������������� �� �����-���� �����, �� ������ ��� ��������� �����, ���
 * �� ���� �����������, ��������� ��-�� ������������� ������������ �����������
 * ��� ���� �������� � ����������� ������������.
 *
 *                              2019 �� "�����"
 *==============================================================================
 */

#include "buffer.h"

extern uint32_t SystemCoreClock;
volatile char RX_FLAG_END_LINE = 0;
volatile char RX_FLAG_END_PKT = 0;
volatile char RX_FLAG_Timeout = 0;
volatile unsigned int RX_pktsize = 2;
volatile unsigned char RXi;
volatile unsigned char RXc;
volatile unsigned char RXEN;
volatile unsigned char SpiBufferStop;
volatile unsigned char SpiBufferRun;
char RX_BUF[RX_BUF_SIZE] = {'\0'};


/**
  * ������� ������������� ������� ��� ������� ��������� ������
  */
void BufferTimerInit()
{
  RCU->PCLKCFG_bit.BUFF_TIMEREN = 1;
	RCU->PCLKCFG_bit.BUFF_TIMEREN = 1;
	// ��������� ������� ������� ��������� ������ - ������ 10��
	BUFF_TIMER->LOAD =  (SystemCoreClock/(BUFF_TIMER_TIMEOUT_MS*1000));
	BUFF_TIMER->VALUE =BUFF_TIMER->LOAD;
	NVIC_EnableIRQ(BUFF_TIMER_IRQ);
}

/**
  * ������� ������������� ��������� ������
  */
void Buffer_init(void)
{
  	retarget_init();
	RETARGET_UART->IFLS_bit.RXIFLSEL = 0;
	RETARGET_UART->LCRH_bit.FEN = 0;
	RETARGET_UART->IMSC_bit.RXIM = 1;
  	clear_RXBuffer();
  	RX_FLAG_END_LINE = 0;
  	RX_FLAG_END_PKT = 0;
  	RX_FLAG_Timeout = 0;	
	BufferTimerInit();
	NVIC_EnableIRQ(RETARGET_UART_RX_IRQn);
}

/**
  * ������� ������� ��������� ������
  */
void clear_RXBuffer(void) {
	for (RXi=0; RXi < RX_BUF_SIZE; RXi++)
		RX_BUF[RXi] = '\0';
	RX_BUF[0] = '\0';
	RXi = 0;
}

/**
  * ������� ��������� ���������� �� ������ UART
  */
void RETARGET_UART_RX_IRQHandler(void)
{		

	if (RETARGET_UART->MIS & UART_MIS_RXMIS_Msk) 		
	{
		RXc = retarget_get_char();
		BUFF_TIMER->VALUE = BUFF_TIMER->LOAD; // Timer reset
		  if      (RXc == CHAR_START_PKT) RXEN = 1;
		  else if (RXc == CHAR_END_PKT)   
		  {
			  RX_BUF[RXi++] = '\0';
			  RXEN = 0;	
			  RX_FLAG_END_PKT = 1;
				if(*((uint32_t *)(&RX_BUF)) == BUFF_CMD_STOP)
				{ //������� ������� �������� ���
					SpiBufferStop = 1;
				} else if (SpiBufferRun) clear_RXBuffer(); 	
		  }
		  else 
		  {
		    if (RXi < RX_BUF_SIZE)
			  {
			    if (RXEN) RX_BUF[RXi++] = RXc;
		    } else
   			  RX_FLAG_END_PKT = 1;
		  }
	}	
	RETARGET_UART->ICR = UART_ICR_RXIC_Msk;			
}

/**
  * ������� �������� ������ �� UART
  */
void USARTSend(char *pucBuffer)
{
    while (*pucBuffer)
    {
      retarget_put_char(*pucBuffer++);  
    }
}

/**
  * ������� ����������� �������� ASCII (�������� ������� ����� 
  * � 16-��������� ������� ����������) � �����
  */
uint32_t AsciiHexToInt (char * pbuf, char** upd_pbuf)
{	
	uint8_t  cnt_nibbles = 0;
	uint8_t  cnt_nibbles_store;
	uint32_t result = 0;
	uint8_t  ascii_char;
	uint8_t  ignore_flag = 0;
	
	do
	{
		//if(*pbuf == '\0')
		//	return 0;
		//check correct ASCII HEX symbol (0-9, a-f, A-F)
	  if(((*pbuf >= 0x30) & (*pbuf <= 0x39)) | (((*pbuf & 0x0DF) >= 0x41) & ((*pbuf & 0x0DF) <= 0x46)))
		{
			pbuf++;
			cnt_nibbles++;
			ignore_flag = 1;
		}
		else 
		{
			if(ignore_flag == 1)
				break;
			pbuf++;
		}
	} while (1);
	// update pointer
	if(upd_pbuf != NULL)
		*upd_pbuf = pbuf;	
	//check number of ASCII symbols
	if(cnt_nibbles > 8) cnt_nibbles = 8;
	//store value of number nibbles
	cnt_nibbles_store = cnt_nibbles;
	//compile integer
	for( ; cnt_nibbles > 0; cnt_nibbles--)
	{
		//decr pointer 
		ascii_char = *(--pbuf);
		//check
		if (ascii_char <= 0x39) 
			ascii_char -= 0x30; 
		else if (ascii_char >= 0x61)
			ascii_char -= 0x57;
		else if (ascii_char >= 0x41)
			ascii_char -= 0x37;
		
		result |= (ascii_char << (4*(cnt_nibbles_store-cnt_nibbles))) ;
	}
	return result;
}

/**
  * ������� ����������� �������� ASCII (�������� ������� ����� 
  * � 10-��������� ������� ����������) � �����
  */
uint32_t AsciiDecToInt (char * pbuf, char** upd_pbuf)
{	
	uint32_t digit=1;
	uint8_t  cnt_nibbles = 0;
	uint8_t  cnt_nibbles_store;
	uint32_t result = 0;
	uint8_t  ascii_char;
	uint8_t  ignore_flag = 0;
	
	do
	{
		//if(*pbuf == '\0')
		//	return 0;
		//check correct ASCII HEX symbol (0-9, a-f, A-F)
	  if(((*pbuf >= 0x30) & (*pbuf <= 0x39)))
		{
			pbuf++;
			cnt_nibbles++;
			ignore_flag = 1;
		}
		else 
		{
			if(ignore_flag == 1)
				break;
			pbuf++;
		}
	} while (1);
	// update pointer
	if(upd_pbuf != NULL)
		*upd_pbuf = pbuf;	
	//check number of ASCII symbols
	if(cnt_nibbles > 8) cnt_nibbles = 8;
	//store value of number nibbles
	cnt_nibbles_store = cnt_nibbles;
	//compile integer
	digit = 1;
	for( ; cnt_nibbles > 0; cnt_nibbles--)
	{
		//decr pointer 
		ascii_char = *(--pbuf);
		//check
		if (ascii_char <= 0x39) 
			ascii_char -= 0x30; 
//		result |= (ascii_char << (4*(cnt_nibbles_store-cnt_nibbles))) ;
		result += ascii_char*digit;
		digit = digit*10;
	}
	return result;
}

/**
  * ������� �������� ���������� ������
  */
char isRXActive (void)
{
	return RXEN;
}
/**
  * ������� ���������� ���������� �������� � �������� ������
  */
char GetNBytes (void)
{
		return RXi;
}

/**
  * ������� �������� ��������� �� �������� �����
  */
char* GetPBuf (void)
{
		return &RX_BUF[0];
}

//-- IRQ handlers --------------------------------------------------------------
void TMR0_IRQHandler()
{
  	clear_RXBuffer();
	BUFF_TIMER->INTSTATUS = 1;
}

