/*==============================================================================
 * Функции обработки команд буфера данных для К1921ВК01Т
 *------------------------------------------------------------------------------
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

#include "buff_hndlr.h"
#include "spi.h"
#include "dwt.h"

//char mybuff[128];
extern uint32_t SystemCoreClock;
extern uint8_t Dac_Channel_Mode;
extern volatile unsigned char SpiBufferStop;
extern volatile unsigned char SpiBufferRun;
extern volatile char ledBlock;

uint16_t commands_count;
uint16_t dac_buff_count,dac_buff_maxsize;
Dac_Dat_TypeDef Dac_Buff[DAC_BUFF_SIZE];
uint8_t Dac_DevCurrent = DAC_1273HA034;
uint8_t Dac_LDAC_Mode =0;
uint8_t Dac_MSB =0;
uint8_t Dac_DataLoad_Parallel =0;

uint32_t Dac_Buff_spi[DAC_BUFF_SIZE];

uint8_t spi_scr,spi_cpsr,spi_div;
	

static const uint32_t crc32r_table[] = {
                0x00000000,0x04C11DB7,0x09823B6E,0x0D4326D9,
                0x130476DC,0x17C56B6B,0x1A864DB2,0x1E475005,
                0x2608EDB8,0x22C9F00F,0x2F8AD6D6,0x2B4BCB61,
                0x350C9B64,0x31CD86D3,0x3C8EA00A,0x384FBDBD
};


/**
  * @brief   Задержка времени
  * @param   time  Значение времени в мкс
  * @retval  void
  */
void Delay_us(uint32_t time)
{	
	DWT_Delay_us(time);	
}

/**
  * @brief   Добавление значения точки сигнала в буфер
  * @param   data  Значение точки сигнала
  * @retval  void
  */
void Dac_Buff_add(uint32_t data)
{
  if (dac_buff_count < dac_buff_maxsize)
	{
	  Dac_Buff_spi[dac_buff_count] = data&0x3FFFF;
	  dac_buff_count++;
	}	else printf("Out of Max Count Point %i\r\n",dac_buff_maxsize);	
}	

/**
  * @brief   Установка размера буфер точек сигнала
  * @param   size   Значение размера буфера точек
  * @retval  void
  */
void Dac_Buff_Set_size(uint16_t size)
{
  if (size < DAC_BUFF_SIZE) dac_buff_maxsize = size; 
		else dac_buff_maxsize = DAC_BUFF_SIZE;
	dac_buff_count = 0;
}

/**
  * @brief   Запрос размера буфера точек сигнала
  * @retval  count  Значение размера буфера точек
  */
uint16_t Dac_Buff_Get_Count()
{
	return dac_buff_count;
}

uint32_t Dac_Buff_Get_crc32(uint32_t init_crc, uint8_t *buf, int len)
{
        uint32_t v;
        uint32_t crc;
        crc = ~init_crc;
        while(len > 0) {
                v = *buf++;
                crc = ( crc >> 8 ) ^ crc32r_table[( crc ^ (v ) ) & 0xff];
                len --;
        }
        return ~crc;
}

/**
  * @brief   Вывод списка поддерживаемых команд
  * @retval  void
  */
void InfoCommands()
{
					printf("\nList of CMD: \r\n"); 
					printf("  %s This Command List\r\n",CMD_HELP);
					printf("  %s HW Info: ",CMD_INFO); GET_INFO_VER;
					printf("  %s [Size]: Set size (HEX) of buffer to transmit in DAC (Max = %Xh) \r\n",CMD_BUFF_SIZE,DAC_BUFF_SIZE);  	
					printf("  %s [Data]: Added DWord in buffer\r\n",CMD_BUFF_ADD); 
					printf("  %s: Get Count Data in buffer to transmit in DAC \r\n",CMD_BUFF_COUNT); 	
					printf("  %s: Get CRC32 of loaded Data in buffer to transmit in DAC \r\n",CMD_BUFF_CRC32);	
	
					printf("  %s [Data]: Set speed of SPI (Max = %i)\r\n",CMD_SPI_SETSPEED,(SystemCoreClock >> 1));

					printf("  %s [Data: 0 or 1]: Set LDAC Mode (if 1 - Sync Data on Channels)\r\n",CMD_DAC_UPDATE_SYN);
					printf("  %s [Data: 0 or 1]: Set Parallel Data Load\r\n",CMD_DAC_MODE_LOAD_PARALLEL);
					printf("  %s [Data: 0 or 1]: Set MSB level\r\n",CMD_DAC_MSB);	
					printf("  %s : Reset DAC\r\n",CMD_DAC_RESET);		
	
					printf("  %s: Load buffer into DAC in cicle \r\n",CMD_BUFF_START); 
					printf("  %s: Stop Load buffer into DAC \r\n",CMD_BUFF_STOP); 	

}

/**
  * @brief   Запрос скорости обмена по SPI
  * @retval  Speed  Значение скорости обмена по SPI (бит/с)
  */
uint32_t GetSpiSpeed()
{
  uint32_t tmp;
  if (RCU->SPICFG_bit.DIVEN == 1) spi_div = RCU->SPICFG_bit.DIVN;
    else  spi_div = 1;
	spi_scr = SPI->CR0_bit.SCR;
	spi_cpsr = SPI->CPSR;
  tmp = spi_div*(SPI->CR0_bit.SCR+1)*SPI->CPSR;
  if (tmp >0) return (SystemCoreClock/tmp);
    else return 0;	
}

/**
  * @brief   Вывод содержимого буфера по UART
  * @retval  Speed  Значение скорости обмена по SPI (бит/с)
  */
void Get_Buff(uint8_t *buff,uint8_t *buff_out, uint16_t count)
{
  uint32_t dac_buff_in,dac_buff_out;
	dac_buff_in  = *((uint32_t *) (buff+(count<<2)));
	dac_buff_out = *((uint32_t *) (buff_out+(count<<2)));
	printf("%s %i In: %i; %i; Out: %i; %i\r\n",CMD_BUFF_GET,count,(dac_buff_in >> 16)&0xFFFF,dac_buff_in&0xFFFF,(dac_buff_out >> 16)&0xFFFF,dac_buff_out&0xFFFF);
}


/**
  * @brief   Обработчик команд принятых по UART
  * @retval  void
  */	
void BuffHandler()
{
	uint32_t i;
	char* pbuf;
	uint32_t data = 0;
	uint8_t data_count = 0;
	uint32_t SpiSpeed = 1000000;

	dac_buff_count = 0;
	SpiBufferStop  = 0;
	SpiBufferRun = 0;
	
	DAC1273_Init();
	GET_INFO_VER;
	while (1)
	{
		if(isRXActive() == 0)
		{
			if(GetNBytes() > 0)
			{	
				ledBlock = 1;
				BSP_LED_On();
				/* Обработка команды: Запрос версии программы */
				if CASE_CMD(CMD_INFO)
 				{
					GET_INFO_VER;
				}
				/* Обработка команды: Запрос списка поддерживаемых команд */
				else if CASE_CMD(CMD_HELP)
 				{
					InfoCommands();
				}				
				/* Обработка команды: Запрос размера буфера точек сигнала */
				else if CASE_CMD(CMD_BUFF_COUNT)
 				{
					printf("%s = %i\r\n",CMD_BUFF_COUNT,Dac_Buff_Get_Count());
				}			
				
				else if CASE_CMD(CMD_BUFF_CRC32)
 				{
					printf("%s = %8X\r\n",CMD_BUFF_CRC32,Dac_Buff_Get_crc32(0,(uint8_t *)Dac_Buff_spi, (Dac_Buff_Get_Count() << 2)));
				}						
				/* Обработка команды: Добавление точки сигнала в буфер */	
				else if CASE_CMD(CMD_BUFF_ADD)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_BUFF_ADD);
					data = AsciiHexToInt(pbuf, NULL);
					Dac_Buff_add(data);
						printf("%s %6.0X\r\n",CMD_BUFF_ADD,Dac_Buff_spi[dac_buff_count-1]);	
				}			
				/* Обработка команды: Добавление массива точек сигнала в буфер */
				else if CASE_CMD(CMD_BUFF_ADDN)
				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_BUFF_ADD);
					data_count = AsciiHexToInt(pbuf, &pbuf);
					i = data_count;
					while (i>0)
					{
					  if (i>1) data = AsciiHexToInt(pbuf, &pbuf);
						else data = AsciiHexToInt(pbuf, NULL);
						Dac_Buff_add(data);
						i--;
					}	
					printf("%s Count = %i\r\n",CMD_BUFF_ADDN,data_count);	
				}		
				/* Обработка команды: Установка размера буфера точек сигнала */	
				else if CASE_CMD(CMD_BUFF_SIZE)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_BUFF_SIZE);
					data = AsciiHexToInt(pbuf, NULL);
					Dac_Buff_Set_size(data);
					if (data > dac_buff_maxsize) printf("Out of Max Size %i ",dac_buff_maxsize);
					printf("%s %8X\r\n",CMD_BUFF_SIZE,dac_buff_maxsize);	
				}		
				/* Обработка команды: Запрос скорости обмена по SPI */	
				else if CASE_CMD(CMD_GET_SPISPEED)
  				{
					printf("%s = %i\r\n",CMD_GET_SPISPEED,DAC_SPI_Get_Speed());	
				}		
				/* Обработка команды: Установка скорости обмена по SPI */
				else if CASE_CMD(CMD_SPI_SETSPEED)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_SPI_SETSPEED);
					data = (uint32_t) AsciiDecToInt(pbuf, NULL);	
					SpiSpeed = data;	
					printf("%s (%i) = %i\r\n",CMD_SPI_SETSPEED,data ,DAC_SPI_Set_Speed(data));	
				}							
				/* Обработка команды: Вывод первых N точек буфера по UART */	
				else if CASE_CMD(CMD_BUFF_GET)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_BUFF_GET);
					data = AsciiHexToInt(pbuf, NULL);	
				  if (data < dac_buff_count) Get_Buff((uint8_t *)Dac_Buff,(uint8_t *)Dac_Buff_spi,data);
						else printf("%s Max size of Buffer %i\r\n",CMD_BUFF_GET,dac_buff_count);
				}		
				/* Обработка команды: Вывод всех точек буфера по UART */					
				else if CASE_CMD(CMD_BUFF_ALL_GET)
  				{
						if (dac_buff_count == 0) printf("%s No Data in Buff\r\n",CMD_BUFF_ALL_GET);
						else if (dac_buff_count <= 100) {
							printf("Count Data in Buff: %i\r\n",dac_buff_count);
							for (i=0;i<dac_buff_count;i+=2){
								printf("[%i] = %.5X (%i);\r\n",i,Dac_Buff_spi[i],Dac_Buff_spi[i]);
								Delay_us(1000);
							}	
						}	
				}							
				/* Обработка команды: Управление состоянием вывода MSB ЦАП1273 */						
				else if CASE_CMD(CMD_DAC_MSB)
  				{
					  pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_DAC_MSB);
					  Dac_MSB = ((uint8_t) AsciiDecToInt(pbuf, NULL))&0x01;	
					  if (Dac_MSB) { DAC1273NA_MSB_SET; printf("MSB Enable\r\n");	}
						  else { DAC1273NA_MSB_RESET; printf("MSB Disable\r\n");}	
					}	
				/* Обработка команды: Аппаратный сброс ЦАП1273 */					
				else if CASE_CMD(CMD_DAC_RESET)
  				{
						DAC1273_Reset();
						printf("DAC is Reset\r\n");		
				}		
				/* Обработка команды: Запрос максимально возможного количества точек программмны (зависит от размера ОЗУ) */					
				else if CASE_CMD(CMD_BUFF_MAXPOINT)
				{
					 printf("Max Point: %i\r\n",DAC_BUFF_SIZE);
				}
				else if CASE_CMD(CMD_DAC_DEV_SEL)
				{
				  pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_DAC_DEV_SEL);
					Dac_DevCurrent = ((uint8_t) AsciiDecToInt(pbuf, NULL))&0x0F;
					switch (Dac_DevCurrent) 
					{	
						case DAC_1273HA034: printf("DAC Select Device: 1273HA03A4\r\n");
							break;
						case DAC_1273HA015: printf("DAC Select Device: 1273HA015\r\n");
							break;	
						case DAC_1273HA025: printf("DAC Select Device: 1273HA025\r\n");
							break;							
					}	
				}		
				/* Обработка команды: Управление режимом параллельной работы каналов ЦАП1273 */					
				else if CASE_CMD(CMD_DAC_MODE_LOAD_PARALLEL)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_DAC_MODE_LOAD_PARALLEL);
					Dac_DataLoad_Parallel = ((uint8_t) AsciiDecToInt(pbuf, NULL))&0x01;	
					if (Dac_DataLoad_Parallel) printf("%s Enable\r\n",CMD_DAC_MODE_LOAD_PARALLEL);	
						else printf("%s Disable\r\n",CMD_DAC_MODE_LOAD_PARALLEL);	
				}		
				/* Обработка команды: Управление режимом синхронной загрузки каналов ЦАП1273 */						
				else if CASE_CMD(CMD_DAC_UPDATE_SYN)
  				{
					pbuf = GetPBuf() + BUFF_COMM_SIZE(CMD_DAC_UPDATE_SYN);
					Dac_LDAC_Mode = ((uint8_t) AsciiDecToInt(pbuf, NULL))&0x01;	
					if (Dac_LDAC_Mode) printf("%s Enable\r\n",CMD_DAC_UPDATE_SYN);	
						else printf("%s Disable\r\n",CMD_DAC_UPDATE_SYN);							
				}			
				/* Обработка команды: Запуск кольцевого буфера передачи точек ЦАП1273 */						
				else if CASE_CMD(CMD_BUFF_START)
			  	{
						SpiBufferStop = 0;
						SpiBufferRun = 1;
						clear_RXBuffer();
						NVIC_DisableIRQ(BUFF_TIMER_IRQ);
						if(Dac_DataLoad_Parallel) DAC1273NA_LDAC_RESET;
						  else if (Dac_LDAC_Mode) DAC1273NA_LDAC_SET;
						printf("Start BUFF Write into DAC\r\n");
						if (dac_buff_count == 0) printf("Data Not Load to DAC\r\n");
						else {
							if(Dac_DevCurrent == DAC_1273HA034) SPI_ReInit(SPI_WordLength6b,0,SpiSpeed);
							  else if(Dac_DevCurrent == DAC_1273HA015) SPI_ReInit(SPI_WordLength12b,1,SpiSpeed);
							    else if(Dac_DevCurrent == DAC_1273HA025) SPI_ReInit(SPI_WordLength16b,1,SpiSpeed);
							DAC_SPI_Set_Speed(SpiSpeed);
						
						  if(Dac_DevCurrent == DAC_1273HA034) {
						    if(Dac_LDAC_Mode) DAC1273_Write_Buff_Sync((uint8_t *)Dac_Buff_spi,(dac_buff_count << 2));
						      else DAC1273_Write_Buff((uint8_t *)Dac_Buff_spi,(dac_buff_count << 2));
						  } else if (Dac_DevCurrent == DAC_1273HA015) DAC1273HA015_Write_Buff((uint8_t *)Dac_Buff_spi,(dac_buff_count << 2));
						      else if (Dac_DevCurrent == DAC_1273HA025) DAC1273HA025_Write_Buff((uint8_t *)Dac_Buff_spi,(dac_buff_count << 2));
					  }
						printf("Stop BUFF\r\n");
						SpiBufferRun = 0;	
						NVIC_EnableIRQ(BUFF_TIMER_IRQ);	
				 }	
				/* Обработка команды: Останов работы кольцевого буфера передачи точек ЦАП1273 */				 
				else if CASE_CMD(CMD_BUFF_STOP)
			  	{
						SpiBufferStop = 0;
						SpiBufferRun = 0;
						printf("Stop BUFF\r\n");	
				 }	
				/* Обработка команды: Принята неизвестная команда */					 
				else 
				{	
				  printf("\r\nUNK CMD \r\n");
					InfoCommands();
				}	
				/* Очистка приемного буфера UART */					
				clear_RXBuffer();
				ledBlock = 0;
				BSP_LED_Off();
			}
		}
	}				
}
