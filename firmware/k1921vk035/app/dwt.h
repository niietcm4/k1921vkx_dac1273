/*
  Data Watchpoint and Trace Unit (DWT)
*/

#ifndef __DWT_DELAY_H
#define __DWT_DELAY_H

#include <stdint.h>

#define    DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
#define    DWT_CONTROL   *(volatile unsigned long *)0xE0001000
#define    SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC

#define    DWT_ENABLE 		SCB_DEMCR|=CoreDebug_DEMCR_TRCENA_Msk;// ��������� ������������ DWT

#define    DWT_CLEAR 			DWT_CYCCNT = 0;// �������� ��������

#define    DWT_COUNT_EN		DWT_CONTROL|= DWT_CTRL_CYCCNTENA_Msk; // �������� �������
//������� ������� ��������
#define    DWT_GET_COUNT  DWT_CYCCNT

__STATIC_INLINE void Init_DWT(void)
{ 
	  DWT_ENABLE
		DWT_CLEAR
	  DWT_COUNT_EN
}

__STATIC_INLINE void DWT_Delay_us(uint32_t usec)
{
  uint32_t tic_count = DWT_GET_COUNT + (int)(SystemCoreClock * usec/ 1E6);
	while (DWT_GET_COUNT < tic_count);
}	

__STATIC_INLINE void DWT_Delay_ms(uint32_t msec)
{
  uint32_t tic_count = DWT_GET_COUNT + (int)(SystemCoreClock * msec/ 1E3);
	while (DWT_GET_COUNT < tic_count);
}

#endif /* __DWT_DELAY_H */
