/*==============================================================================
 * Управление ЦАП 1273НА03А4(НИИЭТ) на плате NIIET-BB-K1921VK035
 *------------------------------------------------------------------------------
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru> 
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

#include "system_K1921VK035.h"
#include "retarget_conf.h"
#include "spi.h"
#include "bsp.h"
#include "buffer.h"
#include "buff_hndlr.h"

#define LED_PRESC		8
#define IRQ_EN(NUM) *((volatile unsigned int *)(0xE000E100 + (NUM>>5)*4)) = (1<<(NUM%32));

//-- Variables -----------------------------------------------------------------
// create microrl object and pointer on it

void periph_init()
{
  SystemCoreClockUpdate();
	retarget_init();
	BSP_LED_Init();
	SysTick_Config(10000000);
	BSP_SPI_Init();
	SPI_Init();
	DAC1273_Init();
	Buffer_init();
}

volatile char kdiv;
volatile char ledBlock;

int main()
{ 
	periph_init();
	ledBlock = 0;

	BuffHandler();
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
  kdiv++;
  if (kdiv>LED_PRESC-1){  
	  if (ledBlock == 0) BSP_LED_Toggle();
		kdiv=0;
	}	
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
	printf("Wrong parameters value: file %s on line %d\r\n", file, (int)line);
	while (1)
	{
	}
}/* assert_failed */
#endif/*USE_FULL_ASSERT*/
