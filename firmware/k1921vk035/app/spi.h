/*==============================================================================
 * ļšåäåėåķčß äėß SPI äėß 192101
 *------------------------------------------------------------------------------
 * , ėåźńąķäš ūõķī <dykhno@niiet.ru>
 *==============================================================================
 *     Ē Č,  -
 * ,    ,   
 * ,       
 * ,    .   
 *        
 *     ,    
 * .         
 *   - ,     , 
 *   ,  -   
 *      .
 *
 *                              2019  ""
 *==============================================================================
 */

#ifndef __SPI_H
#define __SPI_H

#include "K1921VK035.h"
#include "system_K1921VK035.h"
#include <stdio.h>

typedef enum
{
    SPI_ModeMaster,       /*!< ģīäóėü SPI ā šåęčģå Slave. */
    SPI_ModeSlave         /*!< ģīäóėü SPI ā šåęčģå Master. */
} SPI_Mode_Typedef;

typedef enum
{
    SPI_SPO_Low,            /*!< ėčķč’ SPI_CLK ā ķčēźīģ óšīāķå. */
    SPI_SPO_High            /*!< ėčķč’ SPI_CLK ā āūńīźīģ óšīāķå. */
} SPI_SPO_Typedef;

typedef enum
{
    SPI_SPH_1Edge,          /*!< āūńņąāėåķčå äąķķūõ ļī ļåšåäķåģó ōšīķņó SPI_CLK. */
    SPI_SPH_2Edge           /*!< āūńņąāėåķčå äąķķūõ ļī ēąäķåģó ōšīķņó SPI_CLK. */
} SPI_SPH_Typedef;

typedef enum
{
    SPI_FRF_SPI_Motorola,   /*!< Ļšīņīźīė SPI Motorola. */
    SPI_FRF_SSI_TI,         /*!< Ļšīņīźīė SSI Texas Instruments. */
    SPI_FRF_Microwire       /*!< Ļšīņīźīė Microwire. */
} SPI_FRF_Typedef;

typedef enum
{
    SPI_TX_Slave_Enable,    /*!< šąēšåųåķą ļåšåäą÷ą äąķķūõ ń āūāīäą SPI_TXD ā šåęčģå slave. */
    SPI_TX_Slave_Disable,   /*!< ēąļšåņ ķą ļåšåäą÷ó ń āūāīäą SPI_TXD ā šåęčģå slave. */
} SPI_TX_Slave_Typedef;

#define SPI_WordLength4b                   ((uint32_t)0x00000003)
#define SPI_WordLength6b                   ((uint32_t)0x00000005)
#define SPI_WordLength8b                   ((uint32_t)0x00000007)
#define SPI_WordLength12b                  ((uint32_t)0x0000000B)
#define SPI_WordLength16b                  ((uint32_t)0x0000000F)

// MOSI - A.1(ALTF3)   SCLK - A.6(ALTF3)  CS# - A.5(GPIO_OUT)  LDAC - A.7(GPIO_OUT)
//#define SPI 					NT_SPI0
//#define SPI_NUM 			0
#define SPI_CLK_DIVEN	NT_COMMON_REG->SPI_CLK_bit.DIVEN_SPI0
#define SPI_CLK_DIV		NT_COMMON_REG->SPI_CLK_bit.DIV_SPI0

#define SPI_SPEED			25000000
		
// DMA SPI
#define SPI_DMA_CHANNEL_NUM (16+SPI_NUM)  
#define SPI_IRQ_DMA_CHANNEL_NUM DMA_Stream16_IRQn
#define SPI_DMA_ENABLE    SPI->SPI_DMACR=SPI_SPI_DMACR_TXDMAE_Msk
#define SPI_DMA_DISABLE   SPI->SPI_DMACR=0

#define DAC_SPI_CPSDVSR			((SystemCoreClock >> 1) / SPI_SPEED)
#define DAC_SPI_WordLength	SPI_WordLength6b
#define DAC_SPI_FRF					SPI_FRF_SPI_Motorola
#define DAC_SPI_SPH					SPI_SPH_2Edge
#define DAC_SPI_SPO					SPI_SPO_Low
#define DAC_SPI_Mode				SPI_ModeMaster

void SPI_Init(void);
uint32_t DAC_SPI_Set_Speed(uint32_t Speed);
uint32_t DAC_SPI_Get_Speed(void);
//uint16_t DAC_SPI_GetFlagStatus(uint16_t SPI_FLAG);
__STATIC_INLINE uint16_t DAC_SPI_GetFlagStatus(uint16_t SPI_FLAG) { if ((SPI -> SR & SPI_FLAG) != 0) return 1; else  return 0; }
void DAC_SPI_putc (unsigned char ch);
uint8_t DAC_SPI_getc (void);
uint16_t DAC_SPI_ReseiveData(void);
//void DAC_SPI_SendData(uint16_t Data);	
__STATIC_INLINE	void DAC_SPI_SendData(uint16_t Data) { SPI -> DR = Data; }
void DAC_CS_LDAC_Init(void);
void SPI_ReInit(uint8_t wlen, uint8_t sph,uint32_t speed);

#endif /* __SPI_H */
