/*==============================================================================
 * Функции управления SPI для К1921ВК01Т
 *------------------------------------------------------------------------------
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

#include "spi.h"

/**
  * @brief   Инициализация SPI
  * @retval  void
  */
void SPI_Init(void)
{
/*Configurate SPI*/
    RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_PLLCLK;
    RCU->SPICFG_bit.CLKEN = 1;
    RCU->SPICFG_bit.RSTDIS = 1;

//    SPI->CPSR = DAC_SPI_CPSDVSR;
//    SPI->CR0_bit.SCR = 9; // SCK=100MHz/((9+1)*50)=200kHz
//    SPI->CR0_bit.FRF = SPI_CR0_FRF_SPI;
//    SPI->CR0_bit.DSS = SPI_CR0_DSS_16bit;
//    SPI->CR1_bit.SSE = 1;	
	
    SPI->CPSR = DAC_SPI_CPSDVSR;
    SPI->CR0 = DAC_SPI_WordLength | (DAC_SPI_SPO << SPI_CR0_SPO_Pos) | (DAC_SPI_SPH << SPI_CR0_SPH_Pos);
    DAC_SPI_Set_Speed(SPI_SPEED);
		//Enable SPI
	  SPI-> CR1 =((DAC_SPI_Mode & 0x1) << SPI_CR1_MS_Pos) | SPI_CR1_SSE_Msk;
}

void SPI_ReInit(uint8_t wlen, uint8_t sph,uint32_t speed)
{
    RCU->SPICFG_bit.CLKEN = 0;
    RCU->SPICFG_bit.RSTDIS = 0;
	
/*Configurate SPI*/
    RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_PLLCLK;
    RCU->SPICFG_bit.CLKEN = 1;
    RCU->SPICFG_bit.RSTDIS = 1;

//    SPI->CPSR = DAC_SPI_CPSDVSR;
//    SPI->CR0_bit.SCR = 9; // SCK=100MHz/((9+1)*50)=200kHz
//    SPI->CR0_bit.FRF = SPI_CR0_FRF_SPI;
//    SPI->CR0_bit.DSS = SPI_CR0_DSS_16bit;
//    SPI->CR1_bit.SSE = 1;	
	
    SPI->CPSR = DAC_SPI_CPSDVSR;
    SPI->CR0 = wlen | (DAC_SPI_SPO << SPI_CR0_SPO_Pos) | (sph << SPI_CR0_SPH_Pos);
    DAC_SPI_Set_Speed(speed);
		//Enable SPI
	  SPI-> CR1 =((DAC_SPI_Mode & 0x1) << SPI_CR1_MS_Pos) | SPI_CR1_SSE_Msk;
}

/**
  * @brief   Вычисление текущей скорости обмена по SPI
  * @retval  Текущее значение скорости обмена по SPI
  */
uint32_t DAC_SPI_Get_Speed(void)
{
  uint32_t tmp;
  if (RCU->SPICFG_bit.DIVEN == 1) tmp = (RCU->SPICFG_bit.DIVN+1) << 1;
    else  tmp = 1;
  tmp = tmp*(SPI->CR0_bit.SCR+1)*SPI->CPSR;
  if (tmp >0) return (SystemCoreClock/tmp);
    else return 0;
}

/**
  * @brief   Установка скорости обмена по SPI
  * @param   Speed  Желаемое значение скорости обмена по SPI  
  * @retval  Возвращает установленное значение скорости обмена по SPI
  */
uint32_t DAC_SPI_Set_Speed(uint32_t Speed)
{
  uint16_t div;
	SystemCoreClockUpdate();
	
	if (Speed <= (SystemCoreClock >> 1)) div = (uint16_t) (SystemCoreClock / Speed); else div = 1;
	if (div % 2 == 0){
	  SPI->CR0_bit.SCR = 0;
		SPI->CPSR = div;
		RCU->SPICFG_bit.DIVEN = 0;
	}	else {
	  SPI->CR0_bit.SCR = div-1;
		SPI->CPSR = 2;
		RCU->SPICFG_bit.DIVEN = 0;	
		
	}
	return (uint32_t) DAC_SPI_Get_Speed();
}

/*__STATIC_INLINE uint16_t DAC_SPI_GetFlagStatus(uint16_t SPI_FLAG)
{
  if ((SPI -> SPI_SR & SPI_FLAG) != 0) return 1;
  else  return 0;
}*/

/**
  * @brief   Передача байта по SPI
  * @param   ch  Байт данных  
  * @retval  void
  */
void DAC_SPI_putc (unsigned char ch)
{   
    while(!(SPI-> SR & SPI_SR_TFE_Msk));
    SPI -> DR = ch;
}

/**
  * @brief   Прием байта по SPI
  * @retval  Принятый байт данных
  */
uint8_t DAC_SPI_getc (void)
{   
    while(!(SPI-> SR & SPI_SR_RNE_Msk));
    return SPI->DR;
}
/**
  * @brief   Чтение регистра данных SPI
  * @retval  Принятые данные
  */
uint16_t DAC_SPI_ReseiveData()
{
  return SPI -> DR;
}

