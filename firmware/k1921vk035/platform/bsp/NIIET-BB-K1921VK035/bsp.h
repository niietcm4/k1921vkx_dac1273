/*==============================================================================
 * Определения для периферии платы NIIET-BB-K1921VK035
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

#ifndef BSP_H
#define BSP_H

#ifdef __cplusplus
extern "C" {
#endif

//-- Includes ------------------------------------------------------------------
#include "K1921VK035.h"
#include <stdio.h>
	
//-- Defines -------------------------------------------------------------------
//LED VH1
#define LED_PORT GPIOA
#define LED_PORT_EN GPIOAEN
#define LED_PIN_POS 15
#define LED_PIN_MSK (1 << LED_PIN_POS)

//Button SB2
#define BTN_PORT GPIOA
#define BTN_PORT_EN GPIOAEN
#define BTN_IRQ_N GPIOA_IRQn
#define BTN_IRQ_HANDLER GPIOA_IRQHandler
#define BTN_PIN_POS 14
#define BTN_PIN_MSK (1 << BTN_PIN_POS)

#define SPI_MOSI_PORT 		GPIOB
#define SPI_MOSI_PORT_EN	GPIOBEN
#define SPI_MOSI_PIN_POS	7
#define SPI_MOSI_PIN_MSK	(1 << SPI_MOSI_PIN_POS)

#define SPI_SCLK_PORT 		GPIOB
#define SPI_SCLK_PORT_EN	GPIOBEN
#define SPI_SCLK_PIN_POS	5
#define SPI_SCLK_PIN_MSK	(1 << SPI_SCLK_PIN_POS)

#define PORT_VALUE_SET		0xFFFF
#define PORT_VALUE_RESET 	0x0000

/* CS# - A.5(GPIO_OUT) */
#define DAC1273NA_CS_PORT				GPIOB
#define DAC1273NA_CS_PORT_EN 	 GPIOBEN
#define DAC1273NA_CS_POS 10
#define DAC1273NA_CS_MSK (1 << DAC1273NA_CS_POS)
#define DAC1273NA_CS_ENABLE			DAC1273NA_CS_PORT->MASKHB[DAC1273NA_CS_MSK >> 8].MASKHB=PORT_VALUE_RESET
#define DAC1273NA_CS_DISABLE		DAC1273NA_CS_PORT->MASKHB[DAC1273NA_CS_MSK >> 8].MASKHB=PORT_VALUE_SET
/* LDAC# - A.7(GPIO_OUT) */
#define DAC1273NA_LDAC_PORT			GPIOB
#define DAC1273NA_LDAC_PORT_EN 	GPIOBEN
#define DAC1273NA_LDAC_POS 11
#define DAC1273NA_LDAC_MSK 			(1 << DAC1273NA_LDAC_POS)
#define DAC1273NA_LDAC_SET			DAC1273NA_LDAC_PORT->MASKHB[DAC1273NA_LDAC_MSK >> 8].MASKHB=PORT_VALUE_SET
#define DAC1273NA_LDAC_RESET		DAC1273NA_LDAC_PORT->MASKHB[DAC1273NA_LDAC_MSK >> 8].MASKHB=PORT_VALUE_RESET
/* RS# - A.8(GPIO_OUT) */
#define DAC1273NA_RS_PORT				GPIOB
#define DAC1273NA_RS_PORT_EN 	 GPIOBEN
#define DAC1273NA_RS_POS 12
#define DAC1273NA_RS_MSK 			(1 << DAC1273NA_RS_POS)
#define DAC1273NA_RS_SET			DAC1273NA_RS_PORT->MASKHB[DAC1273NA_RS_MSK >> 8].MASKHB=PORT_VALUE_SET
#define DAC1273NA_RS_RESET		DAC1273NA_RS_PORT->MASKHB[DAC1273NA_RS_MSK >> 8].MASKHB=PORT_VALUE_RESET
/* MSB - A.0(GPIO_OUT) */
#define DAC1273NA_MSB_PORT			GPIOB
#define DAC1273NA_MSB_PORT_EN 	GPIOBEN
#define DAC1273NA_MSB_POS 13
#define DAC1273NA_MSB_MSK 			(1 << DAC1273NA_MSB_POS)
#define DAC1273NA_MSB_SET			DAC1273NA_MSB_PORT->MASKHB[DAC1273NA_MSB_MSK >> 8].MASKHB=PORT_VALUE_SET
#define DAC1273NA_MSB_RESET		DAC1273NA_MSB_PORT->MASKHB[DAC1273NA_MSB_MSK >> 8].MASKHB=PORT_VALUE_RESET

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init(void);
void BSP_LED_On(void);
void BSP_LED_Off(void);
void BSP_LED_Toggle(void);
void BSP_Btn_Init(void);
uint32_t BSP_Btn_IsPressed(void);

//SPI
void BSP_SPI_Init(void);

#ifdef __cplusplus
}
#endif

#endif // BSP_H
