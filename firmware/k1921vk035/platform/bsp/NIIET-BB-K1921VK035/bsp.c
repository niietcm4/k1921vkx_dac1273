/*==============================================================================
 * Управление периферией платы NIIET-BB-K1921VK035
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "bsp.h"

//-- Private variables ---------------------------------------------------------
static volatile uint32_t btn_press_event = 0;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init()
{
    RCU->HCLKCFG_bit.LED_PORT_EN = 1;
    RCU->HRSTCFG_bit.LED_PORT_EN = 1;
    LED_PORT->DENSET = LED_PIN_MSK;
    LED_PORT->OUTENSET = LED_PIN_MSK;
}

void BSP_LED_Toggle()
{
    LED_PORT->DATAOUTTGL = LED_PIN_MSK;
}

void BSP_LED_On()
{
    LED_PORT->DATAOUTSET = LED_PIN_MSK;
}

void BSP_LED_Off()
{
    LED_PORT->DATAOUTCLR = LED_PIN_MSK;
}

void BSP_Btn_Init()
{
    // Настройка выводов
    RCU->HCLKCFG_bit.BTN_PORT_EN = 1;
    RCU->HRSTCFG_bit.BTN_PORT_EN = 1;
    BTN_PORT->DENSET = BTN_PIN_MSK;
    BTN_PORT->INTTYPESET = (1 << BTN_PIN_POS); // фронт
    BTN_PORT->INTPOLSET = (1 << BTN_PIN_POS);  // положительный
    BTN_PORT->INTENSET = BTN_PIN_MSK;
    NVIC_EnableIRQ(BTN_IRQ_N);
}

uint32_t BSP_Btn_IsPressed()
{
    if (btn_press_event) {
        btn_press_event = 0;
        return 1;
    } else
        return 0;
}

/* Настройка SPI */
void BSP_SPI_Init()
{
    /*GPIO SPI_MOSI*/
    RCU->HCLKCFG_bit.SPI_MOSI_PORT_EN = 1;
    RCU->HRSTCFG_bit.SPI_MOSI_PORT_EN = 1;
    SPI_MOSI_PORT->DENSET = SPI_MOSI_PIN_MSK;
    SPI_MOSI_PORT->ALTFUNCSET = SPI_MOSI_PIN_MSK;
    /*GPIO SPI_SCLK*/
    RCU->HCLKCFG_bit.SPI_SCLK_PORT_EN = 1;
    RCU->HRSTCFG_bit.SPI_SCLK_PORT_EN = 1;
    SPI_SCLK_PORT->DENSET = SPI_SCLK_PIN_MSK;
    SPI_SCLK_PORT->ALTFUNCSET = SPI_SCLK_PIN_MSK;
	  //GPIOA->MASKLB[1].MASKLB
/*Configurate SPI*/
#ifdef SYSCLK_PLL
	RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_PLLCLK;
#else  
	RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_OSECLK;
#endif
    RCU->SPICFG_bit.CLKEN = 1;
    RCU->SPICFG_bit.RSTDIS = 1;
	
//    RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_PLLCLK;
//    RCU->SPICFG_bit.CLKEN = 1;
//    RCU->SPICFG_bit.RSTDIS = 1;
//
//    RCU->HCLKCFG_bit.GPIOBEN = 1;
//    RCU->HRSTCFG_bit.GPIOBEN = 1;
//    //IO config
//    GPIOB->ALTFUNCSET = (GPIO_ALTFUNCSET_PIN5_Msk |
//                         GPIO_ALTFUNCSET_PIN6_Msk |
//                         GPIO_ALTFUNCSET_PIN7_Msk);
//    GPIOB->OUTENSET = GPIO_OUTENSET_PIN4_Msk;
//    GPIOB->DENSET = (GPIO_DENSET_PIN4_Msk | // CS
//                     GPIO_DENSET_PIN5_Msk | // SCK
//                     GPIO_DENSET_PIN6_Msk | // RX
//                     GPIO_DENSET_PIN7_Msk); // TX
//	
}

void DAC_CS_LDAC_Init(void)
{
  /* CS# - A.5(GPIO_OUT) */	
    RCU->HCLKCFG_bit.DAC1273NA_CS_PORT_EN = 1;
    RCU->HRSTCFG_bit.DAC1273NA_CS_PORT_EN = 1;
    DAC1273NA_CS_PORT->DENSET = DAC1273NA_CS_MSK;
    DAC1273NA_CS_PORT->OUTENSET = DAC1273NA_CS_MSK;
/* LDAC - A.7(GPIO_OUT) */	
    RCU->HCLKCFG_bit.DAC1273NA_LDAC_PORT_EN = 1;
    RCU->HRSTCFG_bit.DAC1273NA_LDAC_PORT_EN = 1;
    DAC1273NA_LDAC_PORT->DENSET = DAC1273NA_LDAC_MSK;
    DAC1273NA_LDAC_PORT->OUTENSET = DAC1273NA_LDAC_MSK;
/* RS# - A.8(GPIO_OUT) */	
	  RCU->HCLKCFG_bit.DAC1273NA_RS_PORT_EN = 1;
    RCU->HRSTCFG_bit.DAC1273NA_RS_PORT_EN = 1;
    DAC1273NA_RS_PORT->DENSET = DAC1273NA_RS_MSK;
    DAC1273NA_RS_PORT->OUTENSET = DAC1273NA_RS_MSK;
/* MSB - A.0(GPIO_OUT) */	
	  RCU->HCLKCFG_bit.DAC1273NA_MSB_PORT_EN = 1;
    RCU->HRSTCFG_bit.DAC1273NA_MSB_PORT_EN = 1;
    DAC1273NA_MSB_PORT->DENSET = DAC1273NA_MSB_MSK;
    DAC1273NA_MSB_PORT->OUTENSET = DAC1273NA_MSB_MSK;	
}	


//-- IRQ handlers --------------------------------------------------------------
void BTN_IRQ_HANDLER()
{
    BTN_PORT->INTSTATUS = BTN_PIN_MSK;
    btn_press_event = 1;
}
