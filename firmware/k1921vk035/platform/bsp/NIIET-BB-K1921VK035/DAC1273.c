/*==============================================================================
 * Функции управления ЦАП 1273НА03А4(НИИЭТ) для К1921ВК01Т
 *------------------------------------------------------------------------------
 * НИИЭТ, Александр Дыхно <dykhno@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */
 
#include "DAC1273.h"
#include "bsp.h"
#include "spi.h"
#define DAC_BUFF_SIZE 65536

extern uint32_t Dac_Buff_spi[DAC_BUFF_SIZE];
extern volatile unsigned char SpiBufferStop;

/**
  * @brief   Задержка 50мкс
  * @retval  void
  */
void DAC1273_Delay_50us()
{
  	uint16_t i;
		for (i=0;i<(uint16_t)(SystemCoreClock*50/1000000);i++);
}	

/**
  * @brief   Инициализация сигналов ЦАП
  * @retval  void
  */
void DAC1273_Init()
{
  DAC_CS_LDAC_Init();
  DAC1273NA_CS_DISABLE;
  DAC1273NA_LDAC_RESET;
  DAC1273NA_MSB_RESET;
  DAC1273_Reset();
}
/**
  * @brief   Аппаратный сброс ЦАП
  * @retval  void
  */
void DAC1273_Reset(void)
{
	DAC1273NA_RS_RESET;
	DAC1273_Delay_50us();
	DAC1273NA_RS_SET;
}

/* For 1273HA034*/
/**
  * @brief   Передача данных буфера точек сигнала на вход ЦАП 1273HA034 по SPI
  * @param   buff  Указатель на буфер данных
  * @param   count Количество передаваемых точек сигнала
  * @retval  void
  */
void DAC1273_Write_Buff(uint8_t *buff,uint32_t count)
{
	uint32_t data;
	uint8_t dbyte[2];
	uint8_t *buff_current;
	uint32_t count_current;

	while(SpiBufferStop == 0){
		buff_current = buff;
		count_current = count;
		while (count_current) {
			data = *((uint32_t *) buff_current);
			dbyte[0] = (data >> 12);
			dbyte[1] = (data >> 6);		
	  	DAC1273NA_CS_ENABLE;
			DAC_SPI_SendData(dbyte[0]);
			DAC_SPI_SendData(dbyte[1]);		
	  	DAC_SPI_SendData(data);
	  	buff_current+=4;
    	count_current-=4;		
			while(DAC_SPI_GetFlagStatus(SPI_SR_BSY_Msk)){};
	  	DAC1273NA_CS_DISABLE;
		}	
	}
}

/**
  * @brief   Передача данных буфера точек сигнала на вход ЦАП 1273HA025 по SPI
  * @param   buff  Указатель на буфер данных
  * @param   count Количество передаваемых точек сигнала
  * @retval  void
  */
void DAC1273HA025_Write_Buff(uint8_t *buff,uint32_t count)
{
	uint32_t data;
	uint8_t *buff_current;
	uint32_t count_current;

  while(SpiBufferStop == 0){
		buff_current = buff;
		count_current = count;
		while (count_current) {
			data = *((uint32_t *) buff_current) &0xFFFF;
	  	DAC1273NA_CS_ENABLE;
	  	DAC_SPI_SendData(data);
	  	buff_current+=4;
	  	count_current-=4;		
	  	while(DAC_SPI_GetFlagStatus(SPI_SR_BSY_Msk)){};
	  	DAC1273NA_CS_DISABLE;
		}	
	}
}

/**
  * @brief   Передача данных буфера точек сигнала на вход ЦАП 1273HA015 по SPI
  * @param   buff  Указатель на буфер данных
  * @param   count Количество передаваемых точек сигнала
  * @retval  void
  */
void DAC1273HA015_Write_Buff(uint8_t *buff,uint32_t count)
{
	uint32_t data;
	uint16_t dword;
	uint8_t *buff_current;
	uint32_t count_current;
	while(SpiBufferStop == 0){
		buff_current = buff;
		count_current = count;
		while (count_current) {
	  	data = *((uint32_t *) buff_current) &0xFFFF;
			dword = (data >> 12);
	 		DAC1273NA_CS_ENABLE;
			DAC_SPI_SendData(dword);		
	  	DAC_SPI_SendData(data);
	  	buff_current+=4;
    	count_current-=4;		
			while(DAC_SPI_GetFlagStatus(SPI_SR_BSY_Msk)){};
	  	DAC1273NA_CS_DISABLE;
		}	
  }
}

/**
  * @brief   Синхронная передача данных буфера точек сигнала на вход ЦАП 1273HA034 по SPI
  * @param   buff  Указатель на буфер данных
  * @param   count Количество передаваемых точек сигнала
  * @retval  void
  */
void DAC1273_Write_Buff_Sync(uint8_t *buff,uint32_t count)
{
	uint32_t data;
	uint8_t dbyte[2];
	uint8_t ldac_update=0;
	uint8_t *buff_current;
	uint32_t count_current;	
	SPI->CR1_bit.SSE = 0;	
	SPI->CR0_bit.SPH = 0;
	SPI->CR0_bit.DSS = SPI_WordLength6b;	
	SPI->CR1_bit.MS = 1;	
	SPI->CR1_bit.SSE = 1;		
	while(SpiBufferStop == 0){
		buff_current = buff;	
		count_current = count;
		ldac_update=0;
		while (count_current) {
	  	data = *((uint32_t *) buff_current);
			dbyte[0] = (data >> 12);
			dbyte[1] = (data >> 6);	
	  	DAC1273NA_CS_ENABLE;
			DAC_SPI_SendData(dbyte[0]);
			DAC_SPI_SendData(dbyte[1]);		
	  	DAC_SPI_SendData(data);
	  	buff_current+=4;
    	count_current-=4;		
	  	while(DAC_SPI_GetFlagStatus(SPI_SR_BSY_Msk)){};		
	  	DAC1273NA_CS_DISABLE;
			if (ldac_update) {
				DAC1273NA_LDAC_RESET;
				__nop(); __nop(); __nop(); __nop(); __nop();
		  	DAC1273NA_LDAC_SET;	
				ldac_update = 0;
			} else ldac_update++;
		}	
 	}
}

